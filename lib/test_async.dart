import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

bool isLogin = false;

void main() {
  runApp(
    GetMaterialApp(
      initialRoute: App.routeName,
      getPages: [
        GetPage(name: App.routeName, page: () => App()),
      ],
    ),
  );
}

class App extends StatelessWidget {
  static String routeName = "/app";
  final SearchController logic = SearchController();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SearchController>(
      init: logic,
      builder: (logic) {
        return Scaffold(
          body: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextField(
                  onChanged: (str) {
                    logic.updateInputString(str);
                  },
                ),
                // GetX<SearchController>(
                //   builder: (_) {
                //     return Text("输入值文本${_.inputString.value}");
                //   },
                //   init: logic,
                // ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class SearchController extends GetxController {
  final inputString = "".obs;

  @override
  void onInit() {
    ever(inputString, (value) => print("ever -- $value"));
    once(inputString, (value) => print("once -- $value"));
    debounce(inputString, (value) => print("debounce -- $value"));
    interval(inputString, (value) => print("interval -- $value"),
        time: const Duration(seconds: 2));
    super.onInit();
  }

  updateInputString(String text) {
    inputString.value = text;
  }
}
