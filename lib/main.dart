import 'dart:io';

import 'package:bilibili_getx/core/I18n/str_res_keys.dart';
import 'package:bilibili_getx/core/jPush_util/jPush_util.dart';
import 'package:bilibili_getx/core/package_info/package_info_util.dart';
import 'package:bilibili_getx/core/constant_util/router.dart';
import 'package:bilibili_getx/core/service/utils/constant.dart';
import 'package:bilibili_getx/core/constant_util/app_theme.dart';
import 'package:bilibili_getx/ui/shared/image_asset.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:get/get.dart';

import 'core/I18n/string_res.dart';
import 'core/shared_preferences/bilibili_shared_preference.dart';
import 'core/shared_preferences/shared_preference_util.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (kIsWeb) {
    ///屏幕适配初始化&持久化存储初始化
    SharedPreferenceUtil.initSharedPreference();
    PackageInfoUtil.getInstance();
  } else {
    JPushUtil().startJPush();
    await SharedPreferenceUtil.initSharedPreference();
    PackageInfoUtil.getInstance();
    initialization();
  }
  await ScreenUtil.ensureScreenSize();
  runApp(MyApp());
}

///初始化
Future<void> initialization() async {
  if (defaultTargetPlatform == TargetPlatform.android) {
    ///下载（flutter_downloader）
    WidgetsFlutterBinding.ensureInitialized();
    await FlutterDownloader.initialize(
      debug: Constant.isDebug,
      ignoreSsl: true,
    );

    ///手机状态栏的背景颜色及状态栏文字颜色
    // SystemChrome.setSystemUIOverlayStyle(
    //   const SystemUiOverlayStyle(
    //     ///状态栏字体颜色（黑色）
    //     // statusBarIconBrightness: Brightness.dark,
    //
    //     ///状态栏背景色
    //     statusBarColor: Colors.transparent,
    //   ),
    // );

    // ///注册微信
    // WxUtil.wxRegisterWxApi();
    //
    // ///监听微信回调结果
    // WxUtil.wxDebugResult();
  } else if (Platform.isWindows) {
    // initWindowsSize();
  } else if (Platform.isIOS) {}
}

class MyApp extends StatelessWidget
    with RouterConstantUtil, ImageAssets, HYAppTheme {
  final projectName = SR.bilibili.tr;

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      builder: (ctx, child) {
        ///保存本地语言
        String? locale =
            SharedPreferenceUtil.getString(BilibiliSharedPreference.locale);
        if (locale!.isEmpty) {
          SharedPreferenceUtil.setString(BilibiliSharedPreference.locale, "zh");
        }
        return GetMaterialApp(
          key: navigatorKey,
          debugShowCheckedModeBanner: false,
          title: projectName,
          theme: norTheme,

          ///I18n国际化
          translations: StringRes(),
          locale: locale == "en"
              ? const Locale('en', 'US')
              : const Locale('zh', 'CN'),
          fallbackLocale: locale == "en"
              ? const Locale('en', 'US')
              : const Locale('zh', 'CN'),

          ///起始路由
          initialRoute: initialRoute,

          ///路由和绑定
          getPages: getPages,

          ///smartDialog 插件需要初始化
          navigatorObservers: [FlutterSmartDialog.observer],
          builder: FlutterSmartDialog.init(),

          /// 找不到路径跳转
          unknownRoute: unKnownRoute,

          ///DateTimeRange / 复制粘贴选择全部的编辑框中英文
          // localizationsDelegates: const [
          //   GlobalMaterialLocalizations.delegate,
          //   GlobalWidgetsLocalizations.delegate,
          //   GlobalCupertinoLocalizations.delegate,
          //   SfGlobalLocalizations.delegate,
          // ],
        );
      },

      ///键盘
      rebuildFactor: RebuildFactors.always,
    );
  }
}
