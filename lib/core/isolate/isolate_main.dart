import 'dart:io';

import '../debug/debug_util.dart';
main() async {
  // print(1);
  // // Isolate.spawn((message) { }, message)
  // // compute((message) => print(message), 10);
  // // sleep(Duration(seconds: 2));
  // // print(2);
  //
  // for (int i = 0; i < 10; i++) {
  //   Future(() async{
  //     print("$i coming");
  //     await compute(func, i);
  //   }).then((value) => print("$i"));
  // }
  //
  // for (int i = 0; i < 10; i++) {
  //   func(i);
  // }
  // final jsonData = await Isolate.run(() async {
  //   await func(1);
  //   await func(2);
  //   await func(3);
  // });
}

Future<int> func(message) async {
  sleep(const Duration(seconds: 1));
  DU.l(message);
  return 1;
}
