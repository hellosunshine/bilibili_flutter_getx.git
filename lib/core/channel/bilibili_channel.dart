///channel要与native中的channel名称相同
class BilibiliChannel {
  static String uploadChannel = "upload_channel";
  static String stayAliveChannel = "stay_alive_channel";
  static String startServiceChannel = "start_service_channel";
  static String chartChannel = "chart_channel";

  ///开启拍摄媒体功能
  static String takeMediaChannel = "take_media_channel";
}
