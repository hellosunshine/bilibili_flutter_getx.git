import 'package:bilibili_getx/core/debug/debug_util.dart';
import 'package:realm/realm.dart';

import 'modify_realm_model.dart';

main() {
  final config = Configuration.local(
    [ModifyRealmModel.schema],
    // schemaVersion: 1,
    // schemaVersion: 2, //更新版本
    schemaVersion: 3, //更新版本
  );
  final realm = Realm(config);
  realm.write<ModifyRealmModel?>(() {
    realm.add<ModifyRealmModel>(ModifyRealmModel("james", "jock"));
    return null;
  });
  // james
  // jock
  // james
  // jock
  // james
  // jock
  RealmResults<ModifyRealmModel> results = realm.all<ModifyRealmModel>();
  for (var item in results) {
    DU.l(item.firstName);
    DU.l(item.lastName);
    // print(item.age);
  }
}
