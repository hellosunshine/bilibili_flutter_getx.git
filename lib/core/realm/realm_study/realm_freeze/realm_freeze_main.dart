import 'package:bilibili_getx/core/realm/realm_util.dart';
import 'package:realm/realm.dart';

import '../../../debug/debug_util.dart';
import 'realm_freeze_model.dart';

main() {
  ///冻结Realm
  // freezeRealm();

  ///冻结RealmResult
  // freezeRealmResults();

  ///冻结RealmObject
  // freezeRealmObject();

  ///冻结RealmObject的RealmList
  // freezeRealmListInRealmObject();

  ///检查数据是否被冻结
  // checkIfFrozen(RealmUtil().realmInstance);
}

///冻结Realm
void freezeRealm() {
  ///初始化数据
  RealmUtil().realmInstance?.write(() {
    RealmUtil().realmInstance?.add<RealmFreezeModel>(RealmFreezeModel("one"));
  });

  ///冻结Realm
  final frozenRealm = RealmUtil().realmInstance?.freeze();

  ///更新数据
  RealmUtil().realmInstance?.write(() {
    RealmUtil().realmInstance?.add<RealmFreezeModel>(RealmFreezeModel("two"));
  });

  final RealmResults<RealmFreezeModel>? results =
      frozenRealm?.all<RealmFreezeModel>();
  DU.l("冻结Realm");
  // one
  for (var item in results!) {
    DU.l(item.text);
  }

  ///完成冻结的工作后，必须关闭Realm，否则会内存泄漏
  frozenRealm?.close();
}

void freezeRealmResults() {
  /// 初始化数据
  RealmUtil().realmInstance?.write(() {
    RealmUtil().realmInstance?.deleteAll<RealmFreezeModel>();
    RealmUtil().realmInstance?.add<RealmFreezeModel>(RealmFreezeModel("one"));
  });

  ///冻结RealmResults
  RealmResults<RealmFreezeModel>? results =
      RealmUtil().realmInstance?.all<RealmFreezeModel>();
  final frozenRealmResults = results?.freeze();

  ///更新数据
  RealmUtil().realmInstance?.write(() {
    results?.forEach((element) {
      element.text = "1";
    });
  });

  final queryResult = frozenRealmResults?.query(r'text == $0', ["1"]);
  // 0
  DU.l(queryResult?.length);

  ///关闭
  frozenRealmResults?.realm.close();
}

void freezeRealmObject() {
  /// 初始化数据
  RealmUtil().realmInstance?.write(() {
    RealmUtil().realmInstance?.deleteAll<RealmFreezeModel>();
    RealmUtil().realmInstance?.add<RealmFreezeModel>(RealmFreezeModel("one"));
  });

  ///冻结realmObject
  final realmObject = RealmUtil().realmInstance?.all<RealmFreezeModel>().first;
  final frozenRealmObject = realmObject?.freeze();

  ///更改数据
  RealmUtil().realmInstance?.write(() {
    RealmUtil().realmInstance?.delete<RealmFreezeModel>(realmObject!);
  });

  // false
  DU.l(realmObject?.isValid);
  // true
  DU.l(frozenRealmObject?.isValid);

  ///关闭
  frozenRealmObject?.realm.close();
}

///冻结RealmObject中的RealmList
void freezeRealmListInRealmObject() {
  ///初始化数据
  RealmUtil().realmInstance?.write(() {
    RealmUtil().realmInstance?.deleteAll<RealmFreezeModel>();
    RealmUtil().realmInstance?.add<RealmFreezeModel>(RealmFreezeModel(
          "One",
          attributes: ["1", "2"],
        ));
  });

  ///冻结RealmObject的RealmList
  RealmResults<RealmFreezeModel>? results =
      RealmUtil().realmInstance?.all<RealmFreezeModel>();
  final firstResult = results?.first;
  final firstResultRealmList = firstResult?.attributes.freeze();
  RealmUtil().realmInstance?.write(() {
    firstResult?.attributes.add("3");
  });
  final index = firstResultRealmList?.indexOf("3");

  /// -1
  DU.l(index);

  ///关闭
  firstResultRealmList?.realm.close();
}

/// 检查数据是否被冻结
void checkIfFrozen(dynamic object) {
  if (object is Realm) {
    DU.l(object.isFrozen);
  } else if (object is RealmResults) {
    DU.l(object.isFrozen);
  } else if (object is RealmObject) {
    DU.l(object.isFrozen);
  } else if (object is RealmList) {
    DU.l(object.isFrozen);
  } else if (object is RealmSet) {
    DU.l(object.isFrozen);
  } else {
    DU.l("not type");
  }
}
