import 'package:bilibili_getx/core/debug/debug_util.dart';
import 'package:bilibili_getx/core/realm/realm_study/realm_model/realm_model_data_model.dart';
import 'package:realm/realm.dart';

import '../../realm_util.dart';
import '../realm_realmSet/realm_realm_set_model.dart';

main() {
  ///查询所有
  RealmResults<RealmRealmSetModel>? results =
      RealmUtil().realmInstance?.all<RealmRealmSetModel>();

  //新增测试数据
  RealmUtil().realmInstance?.write<RealmModelDataModel?>(() {
    RealmUtil().realmInstance?.deleteAll<RealmModelDataModel>();
    RealmUtil().realmInstance?.add<RealmModelDataModel>(RealmModelDataModel(
          Uuid.v4(),
          id: ObjectId(),
          text: "hello Realm",
          state: true,
          number: 1,
          time: DateTime.now(),
          textNote: "123",
        ));
    return null;
  });

  ///按条件查询
  RealmResults<RealmModelDataModel>? queryResult = RealmUtil()
      .realmInstance
      ?.query<RealmModelDataModel>(r'state == $0', [true]);

  ///{apple, banana, orange}
  // {null, 0, 1}
  // {Instance of 'SomeRealmModel', Instance of 'SomeRealmModel'}
  DU.l("所有结果");
  for (var item in results!) {
    DU.l(item.stringSet);
    DU.l(item.intSet);
    DU.l(item.objectSet);
  }

  ///hello Realm
  DU.l("条件查询");
  for (var item in queryResult!) {
    DU.l(item.text);
  }
}
