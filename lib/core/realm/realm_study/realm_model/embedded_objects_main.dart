import 'package:bilibili_getx/core/debug/debug_util.dart';
import 'package:realm/realm.dart';

import '../../realm_util.dart';
import 'realm_model_data_model.dart';

///存储对象
main() {
  RealmUtil().realmInstance?.write<RealmModelDataModel?>(() {
    RealmModelDataModel realmModelData = RealmModelDataModel(
      Uuid.v4(),
      part: PartModel("part001", "partOne"),
    );
    RealmUtil().realmInstance?.deleteAll<RealmModelDataModel>();
    RealmUtil().realmInstance?.add<RealmModelDataModel>(realmModelData);
    return null;
  });
  final RealmResults<RealmModelDataModel>? results =
      RealmUtil().realmInstance?.all<RealmModelDataModel>();
  // partId: part001
  // partName: partOne
  for (var item in results!) {
    DU.l("partId: ${item.part?.partId}");
    DU.l("partName: ${item.part?.partName}");
  }
}
