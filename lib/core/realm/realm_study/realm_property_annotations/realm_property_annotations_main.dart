import 'package:bilibili_getx/core/debug/debug_util.dart';
import 'package:realm/realm.dart';

import '../../realm_util.dart';
import 'realm_property_annotations_model.dart';

main() {
  RealmUtil().realmInstance?.write<RealmPropertyAnnotationsModel?>(() {
    RealmUtil().realmInstance?.deleteAll<RealmPropertyAnnotationsModel>();
    RealmPropertyAnnotationsModel realmPropertyAnnotations =
        RealmPropertyAnnotationsModel(
      ObjectId(),
      DateTime.now().toString(),
      "text",
      100,
    );
    RealmUtil()
        .realmInstance
        ?.add<RealmPropertyAnnotationsModel>(realmPropertyAnnotations);
    return null;
  });
  RealmResults<RealmPropertyAnnotationsModel>? results =
      RealmUtil().realmInstance?.all<RealmPropertyAnnotationsModel>();
  for (RealmPropertyAnnotationsModel item in results!) {
    DU.l(item.id);
    DU.l(item.defaultValue);
    DU.l(item.quickQueryProperty);
  }
}
