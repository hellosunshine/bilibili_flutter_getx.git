import 'package:realm/realm.dart';

import '../../../debug/debug_util.dart';
import 'car_model.dart';

main() {
  // Realm.deleteRealm("encryptedRealm.realm");
  final key = List<int>.generate(64, (i) => 1);
  final encryptedConfig = Configuration.local([Car.schema],
      encryptionKey: key, path: "encryptedRealm.realm");
  final encryptedRealm = Realm(encryptedConfig);
  DU.l(key);
  DU.l(encryptedRealm.config.path);
}
