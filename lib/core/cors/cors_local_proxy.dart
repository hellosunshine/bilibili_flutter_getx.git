import 'package:shelf/shelf_io.dart' as shelf_io;
import 'package:shelf_proxy/shelf_proxy.dart';

//前端页面访问本地端口号
const String localHost = "127.0.0.1";
const int apiPort = 8002;
const int livePort = 8001;
const int videoPort = 8003;
const int loginPort = 8004;
const int mallPort = 8005;
const int searchPort = 8006;
const int appPort = 8007;
const int showPort = 8008;
const int localPort09 = 8009;

const String appUrl = "https://app.bilibili.com";
const String apiUrl = "https://api.bilibili.com";
const String showUrl = "https://show.bilibili.com";
const String mallUrl = "https://mall.bilibili.com";
const String liveUrl = "https://api.live.bilibili.com";
const String searchUrl = "http://s.search.bilibili.com";
const String passportUrl = "https://passport.bilibili.com";
const String videoUrl = "https://m.bilibili.com";

/// 本地代理，若运行在web端，需要在这个文件下，用命令dart来启动本地代理，处理跨域的问题
/// cd lib/cors
/// dart run cors_local_proxy.dart
void main() async {
  var server01 =
      await shelf_io.serve(proxyHandler(liveUrl), localHost, livePort);
  server01.defaultResponseHeaders.add('Access-Control-Allow-Origin', '*');
  server01.defaultResponseHeaders.add('Access-Control-Allow-Credentials', true);

  var server02 = await shelf_io.serve(proxyHandler(apiUrl), localHost, apiPort);
  server02.defaultResponseHeaders.add('Access-Control-Allow-Origin', '*');
  server02.defaultResponseHeaders.add('Access-Control-Allow-Credentials', true);

  var server03 =
      await shelf_io.serve(proxyHandler(videoUrl), localHost, videoPort);
  server03.defaultResponseHeaders.add('Access-Control-Allow-Origin', '*');
  server03.defaultResponseHeaders.add('Access-Control-Allow-Credentials', true);

  var server04 =
      await shelf_io.serve(proxyHandler(passportUrl), localHost, loginPort);
  server04.defaultResponseHeaders.add('Access-Control-Allow-Origin', '*');
  server04.defaultResponseHeaders.add('Access-Control-Allow-Credentials', true);

  var server05 =
      await shelf_io.serve(proxyHandler(mallUrl), localHost, mallPort);
  server05.defaultResponseHeaders.add('Access-Control-Allow-Origin', '*');
  server05.defaultResponseHeaders.add('Access-Control-Allow-Credentials', true);

  var server06 =
      await shelf_io.serve(proxyHandler(searchUrl), localHost, searchPort);
  server06.defaultResponseHeaders.add('Access-Control-Allow-Origin', '*');
  server06.defaultResponseHeaders.add('Access-Control-Allow-Credentials', true);

  var server07 = await shelf_io.serve(proxyHandler(appUrl), localHost, appPort);
  server07.defaultResponseHeaders.add('Access-Control-Allow-Origin', '*');
  server07.defaultResponseHeaders.add('Access-Control-Allow-Credentials', true);

  var server08 =
      await shelf_io.serve(proxyHandler(showUrl), localHost, showPort);
  server08.defaultResponseHeaders.add('Access-Control-Allow-Origin', '*');
  server08.defaultResponseHeaders.add('Access-Control-Allow-Credentials', true);

  print(
      '$liveUrl -> live Serving at http://${server01.address.host}:${server01.port}\n'
      '$apiUrl -> base Serving at http://${server02.address.host}:${server02.port}\n'
      '$videoUrl -> video Serving at http://${server03.address.host}:${server03.port}\n'
      '$passportUrl -> login Serving at http://${server04.address.host}:${server04.port}\n'
      '$mallUrl -> mall Serving at http://${server05.address.host}:${server05.port}\n'
      '$searchUrl -> search Serving at http://${server06.address.host}:${server06.port}\n'
      '$appUrl -> app Serving at http://${server07.address.host}:${server07.port}\n'
      '$mallUrl -> mall-web Serving at http://${server08.address.host}:${server08.port}\n');
}
