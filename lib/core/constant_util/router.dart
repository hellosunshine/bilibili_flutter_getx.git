import 'package:bilibili_getx/ui/middle_ware/login_auth_middle_ware.dart';
import 'package:bilibili_getx/ui/pages/main/dynamic_circle/dynamic_circle_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/animation_compoent/animation_compoent_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/animation_compoent/animation_compoent_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/animation_study/animation_study_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/animation_study/animation_study_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/canvas_paint_study/canvas_paint_study_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/canvas_paint_study/canvas_paint_study_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/download_file/download_file_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/download_file/download_file_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/flutter_android/flutter_android_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/flutter_android/flutter_android_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/mini_window/mini_window_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/mini_window/mini_window_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/my_we_chat/chat_room/chat_room_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/my_we_chat/chat_room/chat_room_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/my_we_chat/my_we_chat/my_we_chat_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/my_we_chat/my_we_chat/my_we_chat_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/my_we_chat/my_we_chat/wechat_mine/we_chat_setting/we_chat_setting_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/my_we_chat/my_we_chat/wechat_mine/we_chat_setting/we_chat_setting_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/my_we_chat/wechat_login/wechat_login_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/my_we_chat/wechat_login/wechat_login_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/my_we_chat/wechat_register/wechat_register_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/my_we_chat/wechat_register/wechat_register_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/push_message/push_message_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/push_message/push_message_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/qq_share/qq_share_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/qq_share/qq_share_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/scan_qr/scan_qr_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/scan_qr/scan_qr_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/statistics_chart/statistics_chart_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/statistics_chart/statistics_chart_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/video_player_example/video_player_example_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/video_player_example/video_player_example_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/wx_share/wx_share_binding.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/wx_share/wx_share_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/home_binding.dart';
import 'package:bilibili_getx/ui/pages/main/live_play/live_play_binding.dart';
import 'package:bilibili_getx/ui/pages/main/live_play/live_play_view.dart';
import 'package:bilibili_getx/ui/pages/main/mall/mall_binding.dart';
import 'package:bilibili_getx/ui/pages/main/mine/mine_binding.dart';
import 'package:bilibili_getx/ui/pages/main/mine/scan_login/scan_login_binding.dart';
import 'package:bilibili_getx/ui/pages/main/mine/scan_login/scan_login_view.dart';
import 'package:bilibili_getx/ui/pages/main/publish/publish_binding.dart';
import 'package:bilibili_getx/ui/pages/main/publish/publish_view.dart';
import 'package:bilibili_getx/ui/pages/main/publish/upload/bai_du_map_location/bai_du_map_location_binding.dart';
import 'package:bilibili_getx/ui/pages/main/publish/upload/bai_du_map_location/bai_du_map_location_view.dart';
import 'package:bilibili_getx/ui/pages/main/publish/upload/pre_edit_video/pre_edit_video_binding.dart';
import 'package:bilibili_getx/ui/pages/main/publish/upload/pre_edit_video/pre_edit_video_view.dart';
import 'package:bilibili_getx/ui/pages/main/publish/upload/pre_publish_video/pre_publish_video_binding.dart';
import 'package:bilibili_getx/ui/pages/main/publish/upload/pre_publish_video/pre_publish_video_view.dart';
import 'package:bilibili_getx/ui/pages/main/publish/upload/search_location/search_location_binding.dart';
import 'package:bilibili_getx/ui/pages/main/publish/upload/search_location/search_location_view.dart';
import 'package:bilibili_getx/ui/pages/unknown/unknown_binding.dart';
import 'package:bilibili_getx/ui/pages/unknown/unknown_view.dart';
import 'package:get/get.dart';
import '../../ui/pages/bilibili_test/bilibili_test_binding.dart';
import '../../ui/pages/bilibili_test/bilibili_test_view.dart';
import '../../ui/pages/main/home/login/login_binding.dart';
import '../../ui/pages/main/home/login/login_view.dart';
import '../../ui/pages/main/home/search/search_binding.dart';
import '../../ui/pages/main/home/search/search_view.dart';
import '../../ui/pages/main/main_binding.dart';
import '../../ui/pages/main/main_view.dart';
import '../../ui/pages/video_play/video_play_binding.dart';
import '../../ui/pages/video_play/video_play_view.dart';

///定义页面的路由
mixin RouterConstantUtil {
  ///初始路由
  final String initialRoute = MainView.routeName;

  /// 找不到路径
  final GetPage unKnownRoute = GetPage(
    name: UnknownView.routeName,
    page: () => UnknownView(),
    binding: UnknownBinding(),
  );
  final List<GetPage> getPages = [
    ///起始路由
    GetPage(
      name: MainView.routeName,
      page: () => MainView(),
      bindings: [
        MainBinding(),
        HomeBinding(),
        MallBinding(),
        DynamicCircleBinding(),
        MineBinding(),
      ],
      children: [
        ///发布界面
        GetPage(
          name: PublishView.routeName,
          page: () => PublishView(),
          binding: PublishBinding(),

          /// 中间件（登录）
          middlewares: [
            LoginAuthMiddleWare(),
          ],
        ),

        ///搜索界面
        GetPage(
          name: HomeSearchView.routeName,
          page: () => HomeSearchView(),
          binding: SearchBinding(),
        ),
      ],
      transition: Transition.fadeIn,
    ),

    ///登录界面
    GetPage(
      name: BilibiliLoginView.routeName,
      page: () => BilibiliLoginView(),
      binding: LoginBinding(),
    ),

    ///扫描登录
    GetPage(
        name: ScanLoginScreen.routeName,
        page: () => ScanLoginScreen(),
        binding: ScanLoginBinding()),

    ///视频播放界面
    GetPage(
        name: VideoPlayScreen.routeName,
        page: () => const VideoPlayScreen(),
        binding: VideoPlayBinding()),

    ///预编辑界面
    GetPage(
        name: PreEditVideoScreen.routeName,
        page: () => const PreEditVideoScreen(),
        binding: PreEditVideoBinding()),

    ///预编辑界面
    GetPage(
        name: PrePublishVideoScreen.routeName,
        page: () => PrePublishVideoScreen(),
        binding: PrePublishVideoBinding()),

    ///百度地图定位界面
    GetPage(
        name: BaiDuMapLocationScreen.routeName,
        page: () => BaiDuMapLocationScreen(),
        binding: BaiDuMapLocationBinding()),

    ///搜索地名
    GetPage(
        name: SearchLocationScreen.routeName,
        page: () => SearchLocationScreen(),
        binding: SearchLocationBinding()),

    ///搜索地名
    GetPage(
        name: PushMessageScreen.routeName,
        page: () => PushMessageScreen(),
        binding: PushMessageBinding()),

    ///搜索地名
    GetPage(
        name: BilibiliTestScreen.routeName,
        page: () => BilibiliTestScreen(),
        binding: BilibiliTestBinding()),

    ///QQ分享
    GetPage(
        name: QqShareView.routeName,
        page: () => QqShareView(),
        binding: QqShareBinding()),

    ///rive lottie 动画组件
    GetPage(
        name: AnimationCompoentView.routeName,
        page: () => AnimationCompoentView(),
        binding: AnimationCompoentBinding()),

    ///统计图表
    GetPage(
        name: StatisticsChartView.routeName,
        page: () => StatisticsChartView(),
        binding: StatisticsChartBinding()),

    ///直播间
    GetPage(
        name: LivePlayView.routeName,
        page: () => LivePlayView(),
        binding: LivePlayBinding()),

    ///微信分享
    GetPage(
        name: WxShareView.routeName,
        page: () => WxShareView(),
        binding: WxShareBinding()),

    ///视频小窗口
    GetPage(
        name: MiniWindowView.routeName,
        page: () => MiniWindowView(),
        binding: MiniWindowBinding()),

    ///视频播放界面测试样例
    GetPage(
        name: VideoPlayerExampleView.routeName,
        page: () => VideoPlayerExampleView(),
        binding: VideoPlayerExampleBinding()),

    ///画布
    GetPage(
        name: CanvasPaintStudyView.routeName,
        page: () => CanvasPaintStudyView(),
        binding: CanvasPaintStudyBinding()),

    ///更新APP & 下载
    GetPage(
        name: DownloadFileView.routeName,
        page: () => const DownloadFileView(),
        binding: DownloadFileBinding()),

    ///Android原生
    GetPage(
        name: FlutterAndroidView.routeName,
        page: () => FlutterAndroidView(),
        binding: FlutterAndroidBinding()),

    ///扫描二维码
    GetPage(
        name: ScanQrView.routeName,
        page: () => ScanQrView(),
        binding: ScanQrBinding()),

    ///动画
    GetPage(
      name: AnimationStudyView.routeName,
      page: () => AnimationStudyView(),
      binding: AnimationStudyBinding(),
    ),

    ///myWechat
    GetPage(
      name: MyWeChatView.routeName,
      page: () => MyWeChatView(),
      binding: MyWeChatBinding(),
    ),

    ///chatRoom
    GetPage(
      name: ChatRoomView.routeName,
      page: () => ChatRoomView(),
      binding: ChatRoomBinding(),
    ),

    ///仿微信登录界面
    GetPage(
      name: WechatLoginView.routeName,
      page: () => WechatLoginView(),
      binding: WechatLoginBinding(),
    ),

    ///仿微信注册界面
    GetPage(
      name: WechatRegisterView.routeName,
      page: () => WechatRegisterView(),
      binding: WechatRegisterBinding(),
    ),

    ///微信设置界面
    GetPage(
      name: WeChatSettingView.routeName,
      page: () => WeChatSettingView(),
      binding: MyWeChatSettingBinding(),
    ),
  ];
}
