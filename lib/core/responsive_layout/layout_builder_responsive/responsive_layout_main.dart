import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: MyWidget(),
        ),
      ),
    );
  }
}

class MyWidget extends StatelessWidget {
  final Widget mobileWidget = Column(
    children: [
      Expanded(
        flex: 2,
        child: Container(
          alignment: Alignment.center,
          color: Colors.blueAccent,
          child: const Text(
            'Mobile',
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
      Expanded(
        flex: 5,
        child: Container(
          alignment: Alignment.center,
          color: Colors.blueAccent,
          child: const Text(
            'Mobile',
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
      Expanded(
        flex: 7,
        child: Container(
          alignment: Alignment.center,
          color: Colors.blueAccent,
          child: const Text(
            'Mobile',
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    ],
  );

  final Widget tabletWidget = Column(children: [
    Expanded(
      flex: 5,
      child: Row(children: [
        Expanded(
          flex: 3,
          child: Container(
            alignment: Alignment.center,
            color: Colors.blueAccent,
            child: const Text('Tablet', style: TextStyle(fontSize: 24)),
          ),
        ),
        Expanded(
          flex: 5,
          child: Container(
            alignment: Alignment.center,
            color: Colors.grey,
            child: const Text('Tablet', style: TextStyle(fontSize: 24)),
          ),
        ),
      ]),
    ),
    Expanded(
      flex: 3,
      child: Container(
        alignment: Alignment.center,
        color: Colors.green,
        child: const Text('Tablet', style: TextStyle(fontSize: 24)),
      ),
    ),
  ]);

  final Widget desktopWidget = Row(children: [
    Expanded(
      flex: 4,
      child: Container(
        alignment: Alignment.center,
        color: Colors.blueAccent,
        child: const Text('Desktop', style: TextStyle(fontSize: 24)),
      ),
    ),
    Expanded(
      flex: 7,
      child: Container(
        alignment: Alignment.center,
        color: Colors.grey,
        child: const Text('Desktop', style: TextStyle(fontSize: 24)),
      ),
    ),
    Expanded(
      flex: 10,
      child: Container(
        alignment: Alignment.center,
        color: Colors.green,
        child: const Text('Desktop', style: TextStyle(fontSize: 24)),
      ),
    ),
  ]);

  MyWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Responsive(
      mobile: mobileWidget,
      tablet: tabletWidget,
      desktop: desktopWidget,
    );
  }
}

class Responsive extends StatelessWidget {
  final Widget mobile;
  final Widget tablet;
  final Widget desktop;

  const Responsive({
    super.key,
    required this.mobile,
    required this.tablet,
    required this.desktop,
  });

  static bool isMobile(BuildContext context) =>
      MediaQuery.of(context).size.width < 650;

  static bool isTablet(BuildContext context) =>
      MediaQuery.of(context).size.width >= 650;

  static bool isDesktop(BuildContext context) =>
      MediaQuery.of(context).size.width >= 1100;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (ctx, cons) {
        if (cons.maxWidth >= 1100) {
          return desktop;
        } else if (cons.maxWidth >= 650) {
          return tablet;
        } else {
          return mobile;
        }
      },
    );
  }
}
