import 'package:permission_handler/permission_handler.dart';

import '../../ui/widgets/widget_factory/abstract_factory/widget_factory_singleton.dart';

///权限获取
class BilibiliPermission with WidgetFactoryPlugin {
  BilibiliPermission._internal();

  static final BilibiliPermission _instance = BilibiliPermission._internal();

  factory BilibiliPermission() => _instance;

  ///权限获取
  Future<bool> requestPhotosPermissions() async {
    var statue = await Permission.photos.status;
    if (statue == PermissionStatus.granted) {
      return true;
    } else {
      statue = await Permission.photos.request();
      if (statue == PermissionStatus.granted) {
        return true;
      } else {
        openAppSetting();
        return false;
      }
    }
  }

  ///权限获取
  Future<bool> requestStoragePermissions() async {
    var statue = await Permission.storage.status;
    if (statue == PermissionStatus.granted) {
      return true;
    } else {
      statue = await Permission.storage.request();
      if (statue == PermissionStatus.granted) {
        return true;
      } else {
        openAppSetting();
        return false;
      }
    }
  }

  ///定位
  Future<bool> requestBaiDuMapLocationPermissions() async {
    var statue = await Permission.location.status;
    if (statue == PermissionStatus.granted) {
      return true;
    } else {
      statue = await Permission.location.request();
      if (statue == PermissionStatus.granted) {
        return true;
      } else {
        openAppSetting();
        return false;
      }
    }
  }

  ///蓝牙
  Future<bool> requestBlueToothPermissions() async {
    var statue = await Permission.bluetooth.status;
    if (statue == PermissionStatus.granted) {
      return true;
    } else {
      statue = await Permission.bluetooth.request();
      if (statue == PermissionStatus.granted) {
        return true;
      } else {
        openAppSetting();
        return false;
      }
    }
  }

  void openAppSetting() {
    openAppSettings();
  }
}
