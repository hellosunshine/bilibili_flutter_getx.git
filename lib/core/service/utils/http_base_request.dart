import 'dart:io';

import 'package:bilibili_getx/core/debug/debug_util.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';

import 'constant.dart';

String ua =
    "Mozilla/5.0 (Linux; Android 5.1.1; Android SDK built for x86 Build/LMY48X) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/39.0.0.0 Mobile Safari/537.36";

///请求utf8的数据
Map<String, dynamic> utf8Headers = {
  HttpHeaders.contentTypeHeader: 'multipart/form-data',
};

///请求json数据
Map<String, dynamic> jsonHeaders = {
  HttpHeaders.acceptHeader: 'application/json,*/*',
  HttpHeaders.contentTypeHeader: 'application/json',
  HttpHeaders.userAgentHeader: ua,
  HttpHeaders.accessControlAllowOriginHeader: '*',
  HttpHeaders.cookieHeader:
      "buvid3=84F6E053-B579-35E2-C75E-9F554100BE6834154infoc; i-wanna-go-back=-1; _uuid=94524A93-F3710-A7AD-969A-4910FE1010C4FB1033966infoc; buvid4=77036CCD-6634-BC61-5A85-E99F6702D17E34460-022081609-BvEob4vLVHw3RzM780E09A%3D%3D; nostalgia_conf=-1; buvid_fp_plain=undefined; b_ut=5; CURRENT_BLACKGAP=0; fingerprint3=c8c54833688179bbaac5bb4d4247ca3a; PVID=1; b_nut=100; CURRENT_FNVAL=4048; rpdid=|(u|JRu)uJ~R0J'uYYmRR|Ymk; hit-new-style-dyn=0; hit-dyn-v2=1; innersign=0; b_lsid=62951467_184CC882FE4; sid=65oauv6u; bp_video_offset_243766934=734619628907528200; fingerprint=1b0d06d7ec4bc1a0d1038985b61675b7; buvid_fp=1b0d06d7ec4bc1a0d1038985b61675b7; DedeUserID=243766934; DedeUserID__ckMd5=ad56d1c5d71807ca; SESSDATA=09e12e84%2C1685436133%2C51255*c1; bili_jct=fb776c7f2631ed9a402b09211f24a58a"
};

///请求xml数据
Map<String, dynamic> xmlHeaders = {
  HttpHeaders.acceptHeader: 'application/xml,*/*',
  HttpHeaders.contentTypeHeader: 'application/xml;charset=UTF-8',
  HttpHeaders.userAgentHeader: ua,
  HttpHeaders.accessControlAllowOriginHeader: '*',
};

///请求protobuf数据
Map<String, dynamic> protoHeaders = {
  HttpHeaders.acceptHeader: '*/*',
  HttpHeaders.contentTypeHeader: 'application/x-protobuf',
  HttpHeaders.userAgentHeader: ua,
  HttpHeaders.accessControlAllowOriginHeader: '*',
};

///请求json数据
Map<String, dynamic> jsonWebHeaders = {};

///请求xml数据
Map<String, dynamic> xmlWebHeaders = {
  HttpHeaders.acceptHeader: 'application/xml,*/*',
  HttpHeaders.contentTypeHeader: 'application/xml;charset=UTF-8',
  HttpHeaders.userAgentHeader: ua,
  HttpHeaders.accessControlAllowOriginHeader: '*',
};

///请求protobuf数据
Map<String, dynamic> protoWebHeaders = {
  HttpHeaders.acceptHeader: '*/*',
  HttpHeaders.contentTypeHeader: 'application/x-protobuf',
  HttpHeaders.userAgentHeader: ua,
  HttpHeaders.accessControlAllowOriginHeader: '*',
};

class HttpBaseRequest {
  ///网络请求实例
  late Dio _dio;

  ///单例模式
  HttpBaseRequest._internal() {
    _dio = Dio();
  }

  static final _instance = HttpBaseRequest._internal();

  factory HttpBaseRequest() => _instance;

  ///baseUrl为基本url，url为参数部分（get请求），和在一起用
  Future request(
    String baseUrl,
    String url, {
    method,
    params,
    inter,
    contentType,
    responseType,
  }) async {
    ///默认情况下为GET请求
    method = method ?? 'GET';

    ///请求json格式数据
    contentType = contentType ?? 'JSON';

    ///响应json数据
    responseType = responseType ?? ResponseType.json;

    BaseOptions baseOption = BaseOptions(
      method: method,
      baseUrl: baseUrl,
      responseType: responseType,
    );

    _dio.options = baseOption;

    ///请求头
    Map<String, dynamic> httpHeaders;

    if (!kIsWeb) {
      ///手机端
      if (contentType == 'XML') {
        httpHeaders = xmlHeaders;
      } else if (contentType == 'PROTO') {
        ///弹幕
        httpHeaders = protoHeaders;
      } else if (contentType == 'UTF-8') {
        ///登录
        httpHeaders = utf8Headers;
      } else {
        httpHeaders = jsonHeaders;
      }
    } else {
      if (contentType == 'XML') {
        httpHeaders = xmlWebHeaders;
      } else if (contentType == 'PROTO') {
        ///弹幕
        httpHeaders = protoWebHeaders;
      } else {
        httpHeaders = jsonWebHeaders;
      }
    }

    ///全局拦截器
    ///创建默认的拦截器
    Interceptor dInter = InterceptorsWrapper(onRequest: (options, handler) {
      // print("请求拦截");
      return handler.next(options);
    }, onError: (error, handle) {
      // print("错误拦截");
      return handle.next(error);
    }, onResponse: (response, handler) {
      // print("响应拦截");
      handler.next(response);
    });
    List<Interceptor> inters = [dInter];
    //请求单独拦截器
    if (inter != null) {
      inters.add(inter);
    }
    _dio.interceptors.addAll(inters);

    ///打印请求的基本信息
    if (Constant.isDebug) {
      DU.l("-------request begin-------");
      DU.l("URL：${_dio.options.baseUrl + url}");
      DU.l("请求方式：${_dio.options.method}");
      DU.l("请求头：");
      _dio.options.headers.forEach((key, value) {
        DU.l("$key : $value");
      });
      DU.l("响应类型：${_dio.options.responseType}");
      if (method == "POST") {
        DU.l("参数为$params");
      }
    } else {
      DU.l("发布模式");
    }

    ///发送网络请求
    try {
      _dio.options.headers = httpHeaders;
      Response response;
      if (method == "POST") {
        ///post请求
        response = await _dio.request(url, data: params);
      } else {
        ///get请求
        response = await _dio.request(url);
      }
      return response.data;
    } on DioException catch (e) {
      if (e.type == DioExceptionType.connectionError) {
        SmartDialog.showToast("连接错误");
      } else if (e.type == DioExceptionType.cancel) {
        SmartDialog.showToast("取消");
      } else if (e.type == DioExceptionType.badCertificate) {
        SmartDialog.showToast("证书失效");
      } else if (e.type == DioExceptionType.badResponse) {
        SmartDialog.showToast("返回错误");
      } else if (e.type == DioExceptionType.connectionTimeout) {
        SmartDialog.showToast("连接超时");
      } else if (e.type == DioExceptionType.receiveTimeout) {
        SmartDialog.showToast("接收超时");
      } else if (e.type == DioExceptionType.sendTimeout) {
        SmartDialog.showToast("发送超时");
      } else {
        SmartDialog.showToast("未知异常");
        SmartDialog.showToast(e.toString());
      }
      DU.l(e.toString());
      return Future.error(e);
    }
  }
}
