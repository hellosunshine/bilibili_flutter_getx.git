import 'package:bilibili_getx/core/cors/cors_local_proxy.dart';
import 'package:flutter/foundation.dart';

mixin ApiPathUtil {
  static String proxyUrl = "";
  static String baseProxyUrl = "http://127.0.0.1";

  // 本地代理域名 - 解决CorS跨域问题
  String baseUrlApp = kIsWeb ? "$baseProxyUrl:$appPort" : appUrl;
  String baseUrlApi = kIsWeb ? "$baseProxyUrl:$apiPort" : apiUrl;
  String baseUrlShow = kIsWeb ? "$baseProxyUrl:$showPort" : showUrl;
  String baseUrlMall = kIsWeb ? "$baseProxyUrl:$mallPort" : mallUrl;
  String baseUrlLive = kIsWeb ? "$baseProxyUrl:$livePort" : liveUrl;
  String baseUrlSearch = kIsWeb ? "$baseProxyUrl:$searchPort" : searchUrl;
  String baseUrlLogin = kIsWeb ? "$baseProxyUrl:$loginPort" : passportUrl;
  String baseUrlVideo = kIsWeb ? "$baseProxyUrl:$videoPort" : videoUrl;

  String apiPathFeedIndex = "/x/v2/feed/index";
  String apiPathAccountMine = "/x/v2/account/mine";
  String apiPathPolymerWebDynamicV1FeedAll =
      "/x/polymer/web-dynamic/v1/feed/all";
  String apiPathTicketProjectListV2 = "/api/ticket/project/listV2";
  String apiPathSearchSquare = "/x/v2/search/square";
  String apiPathMallCSearchHomeIndexV2 = "/mall-c-search/home/index/v2";
  String apiPathPgcPageBangumi = "/pgc/page/bangumi";
  String apiPathXResourceShowTabV2 = "/x/resource/show/tab/v2";
  String apiPathXLiveWebInterfaceV1IndexGetList =
      "/xlive/web-interface/v1/index/getList?platform=web";
  String apiPathXLiveAppInterfaceV2IndexFeed =
      "/xlive/app-interface/v2/index/feed";
  String apiPathXV2Search = "/x/v2/search";
  String apiPathMainSuggest = "/main/suggest";
  String apiPathXPassportLoginWebKey = "/x/passport-login/web/key";
  String apiPathXPassportLoginLoginSms = "/x/passport-login/login/sms";
  String apiPathXPassportLoginOauth2Login = "/x/passport-login/oauth2/login";
  String apiPathXPassportLoginSmsSend = "/x/passport-login/sms/send";
  String apiPathXRelationStat = "/x/relation/stat";
  String apiPathXSpaceNavNum = "/x/space/navnum";
  String apiPathXTagArchiveTags = "/x/tag/archive/tags";
  String apiPathXWebInterfaceArchiveRelated =
      "/x/web-interface/archive/related";
  String apiPathXV2View = "/x/v2/view";
  String apiPathXV2ReplyMain = "/x/v2/reply/main";
  String apiPathXV2DmListSegSo = "/x/v2/dm/list/seg.so";
}
