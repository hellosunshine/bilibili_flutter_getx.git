import 'package:bilibili_getx/core/service/api_path/api_path_util.dart';

import '../../../ui/shared/params_sign.dart';
import '../../model/other/live_rooms_model.dart';
import '../../model/other/xliveAppInterfaceV2IndexFeedModel.dart';
import '../utils/http_base_request.dart';

///直播界面
class HYLiveRequest with ApiPathUtil {
  HYLiveRequest._internal();

  static final HYLiveRequest _instance = HYLiveRequest._internal();

  factory HYLiveRequest() => _instance;

  Future<HYLiveRoomsModel> getLiveRoomsData() async {
    final result = await HttpBaseRequest()
        .request(baseUrlLive, apiPathXLiveWebInterfaceV1IndexGetList);
    final liveRoomsData = result["data"];
    return HYLiveRoomsModel.fromJson(liveRoomsData);
  }

  Future<XliveAppInterfaceV2IndexFeedModel> getXliveAppInterfaceV2IndexFeedData(
      params) async {
    final url =
        "$apiPathXLiveAppInterfaceV2IndexFeed?${ParamsSign.paramsSerialization(params)}";
    final result = await HttpBaseRequest().request(baseUrlLive, url);
    return XliveAppInterfaceV2IndexFeedModel.fromJson(result);
  }
}
