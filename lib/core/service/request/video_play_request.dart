import 'package:bilibili_getx/core/model/android/video_play/video_profile_model.dart';
import 'package:bilibili_getx/core/service/api_path/api_path_util.dart';
import 'package:bilibili_getx/ui/shared/params_sign.dart';

import '../../model/other/relation_stat_model_model.dart';
import '../../model/other/space_nav_num_model.dart';
import '../../model/other/tag_archive_tags_model.dart';
import '../../model/other/video_model.dart';
import '../../model/other/video_reply_model.dart';
import '../utils/http_base_request.dart';

/// 视频播放界面需要用到的数据
class HYVideoRequest with ApiPathUtil {
  HYVideoRequest._internal();

  static final HYVideoRequest _instance = HYVideoRequest._internal();

  factory HYVideoRequest() => _instance;

  Future<HYRelationStatModel> getRelationStatData(int mid) async {
    final url = "$apiPathXRelationStat?vmid=$mid&jsonp=jsonp";
    final result = await HttpBaseRequest().request(baseUrlApi, url);
    return HYRelationStatModel.fromJson(result);
  }

  Future<HYSpaceNavNumModel> getSpaceNavNumData(int mid) async {
    final url = "$apiPathXSpaceNavNum?mid=$mid";
    final result = await HttpBaseRequest().request(baseUrlApi, url);
    return HYSpaceNavNumModel.fromJson(result);
  }

  Future<List<HYTagArchiveTagsModel>> getTagArchiveTagsData(int aid) async {
    final url = "$apiPathXTagArchiveTags?aid=$aid";
    final result = await HttpBaseRequest().request(baseUrlApi, url);
    final tagArray = result["data"];
    final List<HYTagArchiveTagsModel> tags = [];
    for (var json in tagArray) {
      tags.add(HYTagArchiveTagsModel.fromJson(json));
    }
    return tags;
  }

  Future<List<HYVideoModel>> getArchiveRelatedData(int aid) async {
    final url = "$apiPathXWebInterfaceArchiveRelated?aid=$aid";
    final result = await HttpBaseRequest().request(baseUrlApi, url);
    final relatedVideoArray = result["data"];
    final List<HYVideoModel> relatedVideos = [];
    for (var json in relatedVideoArray) {
      HYVideoModel tempVideo = HYVideoModel.fromJson(json);
      relatedVideos.add(tempVideo);
    }
    return relatedVideos;
  }

  ///获取视频源播放地址（后续需要用正则表达式切出原视频地址）
  Future<String> getMp4VideoPlayData(int aid) async {
    final url = "/video/av$aid.html?from=search";
    final result = await HttpBaseRequest().request(baseUrlVideo, url);
    return result;
  }

  ///获取视频信息
  Future<VideoProfileModel> getVideoView(params) async {
    final url = "$apiPathXV2View?${ParamsSign.paramsSerialization(params)}";
    final result = await HttpBaseRequest().request(baseUrlApp, url);
    return VideoProfileModel.fromJson(result);
  }

  ///回复
  Future<HYVideoReplyModel> getVideoReply(
      String aid, int type, int next) async {
    final url = "$apiPathXV2ReplyMain?oid=$aid&type=$type&next=$next";
    final result = await HttpBaseRequest().request(baseUrlApi, url);
    final videoReplyJson = result["data"];
    return HYVideoReplyModel.fromJson(videoReplyJson);
  }
}
