import 'package:bilibili_getx/core/service/api_path/api_path_util.dart';
import 'package:dio/dio.dart';

import '../../../ui/shared/params_sign.dart';
import '../../model/other/account_mine.dart';
import '../../model/other/hash_key_model.dart';
import '../utils/http_base_request.dart';

/// 登录界面用到的接口
class HYLoginRequest with ApiPathUtil {
  HYLoginRequest._internal();

  static final HYLoginRequest _instance = HYLoginRequest._internal();

  factory HYLoginRequest() => _instance;

  ///获取key和hash（key用于RSA加密,hash为salt值）
  Future<HYHashKeyModel> getPassportLogin() async {
    final url = "$apiPathXPassportLoginWebKey?build=6720300&platform=android";
    final result = await HttpBaseRequest().request(baseUrlLogin, url);
    return HYHashKeyModel.fromJson(result);
  }

  ///短信登录
  Future<String> messageCodeLogin(postBody) async {
    final result = await HttpBaseRequest().request(
      baseUrlLogin,
      apiPathXPassportLoginLoginSms,
      params: postBody,
      method: 'POST',
      contentType: 'UTF-8',
      responseType: ResponseType.plain,
    );
    return result.toString();
  }

  ///密码登录
  Future<String> passwordLogin(postBody) async {
    final result = await HttpBaseRequest().request(
      baseUrlLogin,
      apiPathXPassportLoginOauth2Login,
      params: postBody,
      method: 'POST',
      contentType: 'UTF-8',
      responseType: ResponseType.plain,
    );
    return result.toString();
  }

  ///发送短信
  Future<String> sendSMSMessage(postBody) async {
    final result = await HttpBaseRequest().request(
      baseUrlLogin,
      apiPathXPassportLoginSmsSend,
      params: postBody,
      method: 'POST',
      contentType: 'UTF-8',
      responseType: ResponseType.plain,
    );
    return result.toString();
  }

  ///获取用户的头像、粉丝、关注等基本信息
  Future<HYAccountMineModel?> getAccountMineData(params) async {
    String url =
        "$apiPathAccountMine?${ParamsSign.paramsSerialization(params)}";
    final result = await HttpBaseRequest().request(baseUrlApp, url);
    if (result != null) {
      return HYAccountMineModel.fromJson(result);
    }
    return null;
  }
}
