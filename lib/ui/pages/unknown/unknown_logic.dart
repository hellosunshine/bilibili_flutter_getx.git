import 'package:get/get.dart';

import 'unknown_state.dart';

class UnknownLogic extends GetxController {
  final UnknownState state = UnknownState();
}
