import 'package:bilibili_getx/core/constant_util/app_theme.dart';
import 'package:bilibili_getx/core/constant_util/size.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:get/get.dart';
// import 'unknown_logic.dart';

class UnknownView extends StatelessWidget with SizeConstantUtil, HYAppTheme {
  static String routeName = "/Unknown";

  @override
  Widget build(BuildContext context) {
    // final logic = Get.find<UnknownLogic>();
    // final state = Get.find<UnknownLogic>().state;

    return Scaffold(
      body: Center(
        child: Text(
          "404",
          style: TextStyle(
            fontSize: titleFontSizeCons.r,
            color: norMainThemeColors,
          ),
        ),
      ),
    );
  }
}
