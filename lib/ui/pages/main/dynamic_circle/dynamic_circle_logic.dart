import 'package:bilibili_getx/core/service/request/dynamic_request.dart';
import 'package:get/get.dart';

import 'dynamic_circle_state.dart';

class DynamicCircleLogic extends GetxController {
  final DynamicCircleState state = DynamicCircleState();

  @override
  void onReady() {
    Map<String, dynamic> params = {
      'timezone_offset': '-480',
      'type': 'all',
      'offset': '',
      'page': '1'
    };
    DynamicRequest().getWebDynamicV1FeedAll(params).then((value) {
      if (value != null && value.code == 0) {
        state.dynamicV1FeedAllData = value;
        state.isLoading = false;
        // for (var i = 0; i < value.data!.items.length; i++) {
        // state.keys.add(GlobalKey<ExpandedWidgetState>());
        // state.expandedList.add(false);
        // }
        update();
      }
    });
    super.onReady();
  }

// void expandText(int index) {
// state.expandedList[index] = !state.expandedList[index];
// state.keys[index].currentState!.widgetShift();
// update();
// }
}
