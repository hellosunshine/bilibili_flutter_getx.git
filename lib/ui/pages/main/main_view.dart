import 'package:animations/animations.dart';
import 'package:bilibili_getx/core/platform_design_rules/platform_design_rules.dart';
import 'package:bilibili_getx/ui/pages/bilibili_test/bilibili_test_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/flutter_android/flutter_android_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/push_message/push_message_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/qq_share/qq_share_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/statistics_chart/statistics_chart_view.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/wx_share/wx_share_view.dart';
import 'package:bilibili_getx/ui/widgets/custom/auto_keep_alive_wrapper.dart';
import 'package:bilibili_getx/ui/widgets/custom/bottom_navigation_bar_center_item.dart';
import 'package:flutter/material.dart'
    hide BottomNavigationBarType, BottomNavigationBarItem;
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';

import '../../../core/I18n/str_res_keys.dart';
import '../../../core/constant_util/size.dart';
import '../../../core/shared_preferences/bilibili_shared_preference.dart';
import '../../../core/shared_preferences/shared_preference_util.dart';
import '../../../core/constant_util/app_theme.dart';
import '../../shared/image_asset.dart';
import 'main_logic.dart';

class MainView extends GetView<MainLogic>
    with SizeConstantUtil, HYAppTheme, ImageAssets {
  static const String routeName = "/main";

  @override
  Widget build(BuildContext context) {
    final s = controller.state;
    return AutoKeepAliveWrapper(
      child: Scaffold(
        ///页面切换动画效果
        body: GetBuilder<MainLogic>(builder: (logic) {
          return PageTransitionSwitcher(
            transitionBuilder: (child, animation, secondaryAnimation) {
              return FadeThroughTransition(
                animation: animation,
                secondaryAnimation: secondaryAnimation,
                child: child,
              );
            },
            child: s.mainPages[s.currentIndex],
          );
        }),

        ///底部导航栏
        bottomNavigationBar: GetBuilder<MainLogic>(builder: (logic) {
          return SizedBox(
            height: DesignRules.getInstance().getBottomNavigationBarHeight(),
            child: BottomNavigationBarCenter(
              selectedFontSize: selectFontSizeCons.r,
              unselectedFontSize: unSelectFontSizeCons.r,
              selectedItemColor: norMainThemeColors,
              type: BottomNavigationBarType.fixed,
              currentIndex: s.currentIndex,
              items: [
                _buildBottomNavigationBarItem(SR.home.tr, "home"),
                _buildBottomNavigationBarItem(SR.dynamic.tr, "dynamic"),
                _buildBottomNavigationBarCenterBarItem(),
                _buildBottomNavigationBarItem(SR.mall.tr, "vip"),
                _buildBottomNavigationBarItem(SR.mine.tr, "mine"),
              ],
              onTap: controller.onTapBottomMenu,
            ),
          );
        }),
        floatingActionButton: SpeedDial(
          icon: Icons.star_rate_sharp,
          backgroundColor: norMainThemeColors,
          children: [
            SpeedDialChild(
              onTap: () {
                Get.toNamed(StatisticsChartView.routeName);
              },
              backgroundColor: norWhite01Color,
              label: '统计',
              child: ImageIcon(
                AssetImage(chartsCustomPNG),
                size: 10.r,
              ),
            ),
            SpeedDialChild(
              backgroundColor: norWhite01Color,
              onTap: () {
                Get.toNamed(PushMessageScreen.routeName);
              },
              label: '推送',
              child: Icon(
                Icons.announcement_sharp,
                size: 10.r,
              ),
            ),
            SpeedDialChild(
              backgroundColor: norWhite01Color,
              onTap: () {
                Get.toNamed(QqShareView.routeName);
              },
              label: 'QQ分享',
              child: Icon(
                Icons.share,
                size: 10.r,
              ),
            ),
            SpeedDialChild(
              backgroundColor: norWhite01Color,
              onTap: () {},
              label: '蓝牙',
              child: Icon(
                Icons.bluetooth,
                size: 10.r,
              ),
            ),
            SpeedDialChild(
              backgroundColor: norWhite01Color,
              onTap: () {
                Get.to;
                Get.toNamed(WxShareView.routeName);
              },
              label: '微信分享',
              child: Icon(
                Icons.wechat,
                size: 10.r,
              ),
            ),
            SpeedDialChild(
              backgroundColor: norWhite01Color,
              onTap: () {
                ///切换语言并保存语言至本地
                String? locale = SharedPreferenceUtil.getString(
                    BilibiliSharedPreference.locale);
                if (locale == 'zh') {
                  Get.updateLocale(const Locale('en', 'US'));
                  SharedPreferenceUtil.setString(
                      BilibiliSharedPreference.locale, 'en');
                } else {
                  Get.updateLocale(const Locale('zh', 'CN'));
                  SharedPreferenceUtil.setString(
                      BilibiliSharedPreference.locale, 'zh');
                }
              },
              label: '切换语言',
              child: Icon(
                Icons.abc,
                size: 10.r,
              ),
            ),
            SpeedDialChild(
              backgroundColor: norWhite01Color,
              onTap: () {
                Get.toNamed(BilibiliTestScreen.routeName);
              },
              label: '小窗口',
              child: Icon(
                Icons.desktop_windows_sharp,
                size: 10.r,
              ),
            ),
            SpeedDialChild(
              backgroundColor: norWhite01Color,
              onTap: () {
                Get.toNamed(FlutterAndroidView.routeName);
              },
              label: 'Android原生',
              child: Icon(
                Icons.android_rounded,
                size: 10.r,
              ),
            ),
          ],
        ),
      ),
    );
  }

  ///发布按钮
  BottomNavigationBarItem _buildBottomNavigationBarCenterBarItem() {
    return BottomNavigationBarItem(
      backgroundColor: Colors.red,
      label: null,
      icon: Container(
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 15).r,
        decoration: BoxDecoration(
          color: norMainThemeColors,
          borderRadius: BorderRadius.all(Radius.circular(15.r)),
        ),
        child: Image.asset(
          addCustomPNG,
          width: unselectIconSizeCons.r,
          height: unselectIconSizeCons.r,
        ),
      ),
    );
  }

  ///首页、动态、会员购、我的
  BottomNavigationBarItem _buildBottomNavigationBarItem(
      String title, String iconName) {
    return BottomNavigationBarItem(
      label: title.toUpperCase(),
      icon: Image.asset(
        "assets/image/icon/${iconName}_custom.png",
        width: unselectIconSizeCons.r,
        height: unselectIconSizeCons.r,

        /// 原图片保持不变，直到图片加载完成时替换图片，这样就不会出现闪烁
        gaplessPlayback: true,
      ),
      activeIcon: Image.asset(
        "assets/image/icon/${iconName}_selected.png",
        width: selectIconSizeCons.r,
        height: selectIconSizeCons.r,
        gaplessPlayback: true,
      ),
    );
  }
}
