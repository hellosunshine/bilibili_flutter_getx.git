import 'package:bilibili_getx/core/I18n/str_res_keys.dart';
import 'package:bilibili_getx/core/constant_util/app_theme.dart';
import 'package:bilibili_getx/core/constant_util/size.dart';
import 'package:bilibili_getx/core/model/other/account_mine.dart';
import 'package:bilibili_getx/ui/shared/image_asset.dart';
import 'package:bilibili_getx/ui/widgets/custom/advertising_row.dart';
import 'package:bilibili_getx/ui/widgets/custom/fade_image_default.dart';
import 'package:bilibili_getx/ui/widgets/custom/icon_button_row.dart';
import 'package:bilibili_getx/ui/widgets/custom/user_level.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

import 'mine_logic.dart';
import 'scan_login/scan_login_view.dart';

class MineScreen extends StatefulWidget {
  static const String routeName = "/mine";

  MineScreen({super.key});

  @override
  State<MineScreen> createState() => _MineScreenState();
}

class _MineScreenState extends State<MineScreen>
    with
        AutomaticKeepAliveClientMixin,
        SizeConstantUtil,
        HYAppTheme,
        ImageAssets {
  final logic = Get.find<MineLogic>();
  final state = Get.find<MineLogic>().state;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GetBuilder<MineLogic>(builder: (logic) {
      return SafeArea(
          child: state.finishLoading
              ? Scaffold(
                  appBar: AppBar(
                    backgroundColor: Theme.of(context).primaryColor,
                    elevation: .5,
                    actions: buildMineActions(),
                    bottom: PreferredSize(
                      preferredSize: Size(1.sw, 220.r),
                      child: Container(
                        padding: EdgeInsets.only(
                          left: 15.r,
                          right: 15.r,
                          top: 25.r,
                        ),
                        child: Column(
                          children: [
                            buildMineUserInfoRow(),
                            20.verticalSpace,
                            buildMineUserDetailsRow(),
                            20.verticalSpace,
                            buildMineAppBarFooter(),
                          ],
                        ),
                      ),
                    ),
                  ),
                  body: SingleChildScrollView(
                    controller: ScrollController(),
                    child: buildMineContentBody(),
                  ),
                )
              : Scaffold(
                  body: Center(
                    child: SizedBox(
                      width: 50.r,
                      height: 50.r,
                      child: const CircularProgressIndicator(),
                    ),
                  ),
                ));
    });
  }

  ///按钮列表
  Widget buildMineContentBody() {
    int index = 0;
    List<Widget> widgets = [];
    if (state.isLogin == true) {
      for (var item in state.accountMineData.data!.sectionsV2) {
        ///广告B
        if (index == 3 && state.accountMineData.data!.liveTip != null) {
          widgets.add(buildMineAdvertisingB());
        }
        if (item.items.length == 1) {
          ///广告A
          widgets.add(buildMineAdvertisingA(item));
        } else {
          ///按钮列表顶部的标题
          item.title != null
              ? widgets.add(
                  buildMineTitleAndButton(
                    item.title!,
                    item.button.icon != null
                        ? Container(
                            decoration: BoxDecoration(
                                color: norPink04Colors,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(15.r),
                                )),
                            padding: EdgeInsets.symmetric(
                              horizontal: 12.r,
                              vertical: 6.r,
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                DefaultFadeImage(
                                  imageUrl: item.button.icon!,
                                  width: customFontSizeCons.r,
                                  height: customFontSizeCons.r,
                                ),
                                5.verticalSpace,
                                Text(
                                  item.button.text!,
                                  style: TextStyle(
                                    color: norWhite01Color,
                                    fontSize: customFontSizeCons.r,
                                    fontWeight: FontWeight.normal,
                                    fontFamily: 'bilibiliFonts',
                                  ),
                                )
                              ],
                            ),
                          )
                        : Container(),
                  ),
                )
              : Container();

          ///最后一块采用垂直的排布（联系客服、听视频等）
          if (index == state.accountMineData.data!.sectionsV2.length - 1) {
            widgets.add(
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10.r),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    15.verticalSpace,
                    buildMineMoreServiceItem(
                        item.items[0].icon, item.items[0].title),
                    15.verticalSpace,
                    buildMineMoreServiceItem(
                        item.items[1].icon, item.items[1].title),
                    15.verticalSpace,
                    buildMineMoreServiceItem(
                        item.items[2].icon, item.items[2].title),
                    buildSettingButton(),
                  ],
                ),
              ),
            );
          } else {
            ///按钮列表
            widgets.add(
              Container(
                padding: EdgeInsets.symmetric(vertical: 15.r),
                child: HYIconButtonRow(
                  size: 20.sp,
                  items: item.items,
                ),
              ),
            );
          }
        }
        index++;
      }
      return Container(
        padding: EdgeInsets.all(15.r),
        color: norWhite01Color,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: widgets,
        ),
      );
    } else {
      ///未登录时
      return Container(
        padding: EdgeInsets.all(15.r),
        color: norWhite01Color,
        child: Column(
          children: [
            // buildMineAdvertisingA(accountMineData.data.sectionsV2[0]),
            // Container(
            //   margin: const EdgeInsets.symmetric(vertical: 15).r,
            //   child: HYIconButtonRow(
            //     items: state.accountMineData.data.sectionsV2[0].items,
            //     size: 20.sp,
            //   ),
            // ),
            buildMineTitleAndButton(
                state.accountMineData.data!.sectionsV2[1].title!,
                const Center()),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 15).r,
              child: HYIconButtonRow(
                items: state.accountMineData.data!.sectionsV2[1].items,
                size: iconSizeCons.r,
              ),
            ),
            buildMineTitleAndButton(
                state.accountMineData.data!.sectionsV2[2].title!,
                const Center()),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 10).r,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 15).r,
                    child: buildMineMoreServiceItem(
                        state.accountMineData.data!.sectionsV2[2].items[0].icon,
                        state.accountMineData.data!.sectionsV2[2].items[0]
                            .title),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 15).r,
                    child: buildMineMoreServiceItem(
                        state.accountMineData.data!.sectionsV2[2].items[1].icon,
                        state.accountMineData.data!.sectionsV2[2].items[1]
                            .title),
                  ),
                  buildSettingButton(),
                ],
              ),
            ),
          ],
        ),
      );
    }
  }

  ///设置按钮
  Widget buildSettingButton() {
    return GestureDetector(
      onTap: () {
        ///跳转至设置界面
        // Get.toNamed(HYSettingScreen.routeName);
      },
      child: Container(
        margin: const EdgeInsets.only(top: 15).r,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset(
                  settingPNG,
                  width: customFontSizeCons.r,
                  height: customFontSizeCons.r,
                  color: norMainThemeColors,
                ),
                10.horizontalSpace,
                Text(
                  SR.settings.tr,
                  style: TextStyle(
                    fontSize: customFontSizeCons.r,
                    color: norTextColors,
                  ),
                ),
              ],
            ),
            Icon(
              Icons.arrow_forward_ios,
              size: customFontSizeCons.r,
              color: norGrayColor,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildMineMoreServiceItem(String icon, String text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            DefaultFadeImage(
              imageUrl: icon,
              width: customFontSizeCons.r,
              height: customFontSizeCons.r,
            ),
            10.horizontalSpace,
            Text(
              text,
              style: TextStyle(
                fontSize: customFontSizeCons.r,
                color: norTextColors,
              ),
            ),
          ],
        ),
        Icon(
          Icons.arrow_forward_ios,
          size: customFontSizeCons.r,
          color: norGrayColor,
        ),
      ],
    );
  }

  scan() async {
    var status = await Permission.camera.request();
    if (status.isGranted) {
      Get.toNamed(ScanLoginScreen.routeName);
    }
  }

  List<Widget> buildMineActions() {
    return [
      IconButton(
        onPressed: () {},
        icon: Image.asset(
          bilibiliConnectPNG,
          width: customFontSizeCons.r,
          height: customFontSizeCons.r,
          color: norGrayColor,
        ),
      ),
      IconButton(
        onPressed: () {
          scan();
        },
        icon: Image.asset(
          scanPNG,
          width: customFontSizeCons.r,
          height: customFontSizeCons.r,
          color: norGrayColor,
        ),
      ),
      IconButton(
        onPressed: () {},
        icon: Image.network(
          state.accountMineData.data!.mallHome.icon,
          width: customFontSizeCons.r,
          height: customFontSizeCons.r,
          color: norGrayColor,
        ),
      ),
      IconButton(
        onPressed: () {},
        icon: Image.asset(
          darkModelPNG,
          width: customFontSizeCons.r,
          height: customFontSizeCons.r,
          color: norGrayColor,
        ),
      ),
    ];
  }

  ///用户的粉丝数、动态数、关注数
  Widget buildMineUserDetailsRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        buildUserInfo(
          state.isLogin == false
              ? "—"
              : state.accountMineData.data!.dataDynamic.toString(),
          SR.dynamic,
        ),
        Stack(
          alignment: AlignmentDirectional.center,
          children: [
            Container(
              child: buildUserInfo(
                  state.isLogin == false
                      ? "—"
                      : state.accountMineData.data!.following.toString(),
                  SR.follower),
            ),
            Container(
              height: 20.r,
              padding: EdgeInsets.symmetric(horizontal: 60.r),
              decoration: BoxDecoration(
                  border: Border.symmetric(vertical: BorderSide(width: .2.r))),
            ),
          ],
        ),
        buildUserInfo(
            state.isLogin == false
                ? "—"
                : state.accountMineData.data!.follower.toString(),
            SR.fan),
      ],
    );
  }

  Widget buildUserInfo(String num, String text) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          num,
          style: TextStyle(
            color: norTextColors,
            fontSize: customFontSizeCons.r,
          ),
        ),
        Text(
          text.tr,
          style: TextStyle(
            color: norGrayColor,
            fontSize: customFontSizeCons.r,
          ),
        ),
      ],
    );
  }

  Widget buildMineUserInfoRow() {
    return state.isLogin == true
        ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 50.r,
                width: 50.r,
                child: CircleAvatar(
                  backgroundImage:
                      NetworkImage(state.accountMineData.data!.face),
                ),
              ),
              20.horizontalSpace,
              Expanded(
                child: Stack(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              state.accountMineData.data!.name,
                              style: TextStyle(
                                color: norTextColors,
                                fontSize: customFontSizeCons.r,
                              ),
                            ),
                            5.horizontalSpace,
                            UserLevel(level: state.accountMineData.data!.level)
                          ],
                        ),
                        5.verticalSpace,
                        Container(
                          padding: EdgeInsets.symmetric(
                            horizontal: 2.r,
                            vertical: 2.r,
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(color: norMainThemeColors),
                            borderRadius:
                                BorderRadius.all(Radius.circular(3.r)),
                          ),
                          child: Text(
                            SR.normalVip.tr,
                            style: TextStyle(
                              color: norMainThemeColors,
                              fontSize: customFontSizeCons.r,
                            ),
                          ),
                        ),
                        5.verticalSpace,
                        Text(
                          "${SR.bCoin.tr}: ${state.accountMineData.data!.bcoin}   ${SR.coin.tr}: ${state.accountMineData.data!.coin}",
                          style: TextStyle(
                            color: norGrayColor,
                            fontSize: customFontSizeCons.r,
                          ),
                        )
                      ],
                    ),
                    Positioned(
                      right: 0,
                      top: 0,
                      bottom: 0,
                      child: Row(
                        children: [
                          Text(
                            SR.space.tr,
                            style: TextStyle(
                              color: norGrayColor,
                              fontSize: customFontSizeCons.r,
                            ),
                          ),
                          5.horizontalSpace,
                          Icon(
                            Icons.arrow_forward_ios,
                            size: customFontSizeCons.r,
                            color: norGrayColor,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )
        : Row(
            children: [
              10.horizontalSpace,
              Opacity(
                opacity: .8,
                child: CircleAvatar(
                  radius: 25.r,
                  backgroundColor: norWhite01Color,
                  child: Image.asset(unLoginPNG),
                ),
              ),
              20.horizontalSpace,
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      SR.click2Login.tr.toUpperCase(),
                      style: TextStyle(
                        fontSize: titleFontSizeCons.r,
                        color: norTextColors,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    Icon(
                      Icons.arrow_forward_ios,
                      size: titleFontSizeCons.r,
                      color: norGrayColor,
                    )
                  ],
                ),
              ),
            ],
          );
  }

  ///成为大会员
  Widget buildMineAppBarFooter() {
    return Container(
      width: 1.sw,
      padding: EdgeInsets.symmetric(vertical: 10.r, horizontal: 15.r),
      decoration: BoxDecoration(
        color: norWhite03Color,
        borderRadius: BorderRadius.vertical(top: Radius.circular(4.r)),
        border: Border.all(color: norPink05Colors, width: .5.r),
      ),
      child: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                state.accountMineData.data!.vipSectionV2.title,
                style: TextStyle(
                  color: norPink06Colors,
                  fontSize: titleFontSizeCons.r,
                ),
              ),
              Text(
                state.accountMineData.data!.vipSectionV2.desc,
                style: TextStyle(
                  color: norPink06Colors,
                  fontSize: customFontSizeCons.r,
                ),
              )
            ],
          ),
          Positioned(
            right: 0,
            top: 0,
            bottom: 0,
            child: Icon(
              Icons.arrow_forward_ios,
              size: customFontSizeCons.r,
              color: norPink08Colors,
            ),
          )
        ],
      ),
    );
  }

  ///广告栏A
  Widget buildMineAdvertisingA(SectionsV2 sectionsV2) {
    return AdvertisingRow(
      image: sectionsV2.items[0].commonOpItem!.titleIcon!,
      title: sectionsV2.items[0].commonOpItem!.title!,
      rightBtn: Icon(
        Icons.close,
        size: titleFontSizeCons.r,
        color: norGrayColor,
      ),
    );
  }

  ///广告栏B
  Widget buildMineAdvertisingB() {
    return AdvertisingRow(
      image: state.accountMineData.data!.liveTip!.icon,
      title: state.accountMineData.data!.liveTip!.text,
      rightBtn: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            state.accountMineData.data!.liveTip!.buttonText,
            style: TextStyle(
              color: norMainThemeColors,
              fontSize: customFontSizeCons.r,
            ),
          ),
          state.accountMineData.data!.liveTip!.buttonIcon.isEmpty
              ? Container()
              : DefaultFadeImage(
                  imageUrl: state.accountMineData.data!.liveTip!.buttonIcon,
                  width: customFontSizeCons.r,
                  height: customFontSizeCons.r,
                )
        ],
      ),
    );
  }

  Widget buildMineTitleAndButton(String title, Widget button) {
    return Container(
      padding: EdgeInsets.only(top: 5.r, bottom: 0.r, left: 10.r, right: 5.r),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: TextStyle(
              color: norTextColors,
              fontSize: customFontSizeCons.r,
              fontWeight: FontWeight.normal,
              fontFamily: bF,
            ),
          ),
          button
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
