import 'package:bilibili_getx/ui/pages/main/home/functions/animation_compoent/animation_compoent_logic.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/qq_share/qq_share_logic.dart';
import 'package:get/get.dart';
import 'home/home_logic.dart';
import 'main_logic.dart';

class MainBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => MainLogic());
    Get.lazyPut(() => QqShareLogic());
    Get.lazyPut(() => AnimationCompoentLogic());
  }
}
