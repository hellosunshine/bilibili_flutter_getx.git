import 'package:bilibili_getx/core/constant_util/size.dart';
import 'package:bilibili_getx/ui/shared/shared_util.dart';
import 'package:bilibili_getx/ui/widgets/custom/fade_image_default.dart';
import 'package:bilibili_getx/ui/widgets/custom/highlight_str_in_text.dart';
import 'package:bilibili_getx/ui/widgets/custom/user_level.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

import '../../../../../core/I18n/str_res_keys.dart';
import '../../../../../core/model/other/home_search_result.dart';
import '../../../../../core/constant_util/app_theme.dart';
import '../../../../shared/image_asset.dart';
import '../../../../shared/math_compute.dart';
import 'search_logic.dart';

class HomeSearchView extends GetView<SearchLogic>
    with SizeConstantUtil, HYAppTheme, ImageAssets {
  static String routeName = "/home_search";

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        colorScheme: ColorScheme.light(
          surface: norWhite01Color,
          primary: norMainThemeColors,
        ),
      ),
      child: SafeArea(
        child: GetBuilder<SearchLogic>(
          builder: (logic) {
            return Scaffold(
              backgroundColor: norWhite01Color,
              body: Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.r),
                child: Column(
                  children: [
                    buildSearchTextField(context, controller, controller.state),

                    ///显示热搜还是搜索的结果
                    controller.state.showResult
                        ? buildSearchResult(context, controller.state)
                        : buildSearchRecommend(controller, controller.state),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  ///搜索框
  Widget buildSearchTextField(BuildContext context, logic, state) {
    return Container(
      height: 50.r,
      margin: EdgeInsets.only(bottom: 5.r),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              autofocus: true,
              controller: state.textEditingController,
              cursorColor: norMainThemeColors,
              focusNode: state.focusNode,
              style: TextStyle(
                color: norTextColors,
                fontSize: customFontSizeCons.r,
              ),
              decoration: InputDecoration(
                hintText: SR.searchSomething.tr,
                hintStyle: TextStyle(
                  color: norGrayColor.withOpacity(.6),
                  fontSize: customFontSizeCons.r,
                  fontFamily: bF,
                  fontWeight: FontWeight.normal,
                ),
                isCollapsed: true,
                contentPadding: EdgeInsets.symmetric(
                  horizontal: 30.r,
                  vertical: 12.r,
                ),
                filled: true,
                fillColor: norGrayColor.withOpacity(.15),
                prefixIcon: Image.asset(searchCustomPNG),
                suffixIcon: GestureDetector(
                  onTap: () {
                    logic.clearSearchKey(context);
                  },
                  child: Image.asset(clearSearchPNG),
                ),
                prefixIconConstraints:
                    BoxConstraints(maxHeight: 16.r, minWidth: 50.r),
                suffixIconConstraints:
                    BoxConstraints(maxHeight: 14.r, minWidth: 50.r),
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.all(Radius.circular(50.r)),
                ),
              ),
              onSubmitted: (inputText) {
                if (inputText.isEmpty) {
                  SmartDialog.showToast("搜索内容不能为空");
                } else {
                  logic.finishInputAndStartSearch(inputText);
                }
              },
              onChanged: (inputText) {
                logic.getRecommendSearchKey(inputText);
              },
            ),
          ),
          GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10.r),
              child: Text(
                SR.cancel.tr,
                style: TextStyle(
                  color: norTextColors,
                  fontSize: customFontSizeCons.r,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  ///推荐搜索的关键词
  Widget buildSearchRecommend(logic, state) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.r),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ///b站热搜
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                SR.hotKeys.tr,
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  color: norTextColors,
                  fontSize: customFontSizeCons.r,
                ),
              ),
              Text(
                SR.entireRanking.tr,
                style: TextStyle(
                  color: norGrayColor,
                  fontSize: customFontSizeCons.r,
                ),
              ),
            ],
          ),

          ///热搜列表
          15.verticalSpace,
          state.trendingList.isNotEmpty
              ? GridView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    mainAxisExtent: 20.r,
                    mainAxisSpacing: 15.r,
                    crossAxisSpacing: 15.r,
                    crossAxisCount: 2,
                  ),
                  itemBuilder: (ctx, index) {
                    String searchItemText = state.trendingList[index].showName!;
                    return GestureDetector(
                      onTap: () {
                        logic.finishInputAndStartSearch(searchItemText);
                      },
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(
                            child: Text(
                              noMoreN(searchItemText, 15),
                              style: TextStyle(
                                color: norBlackColors,
                                fontSize: customFontSizeCons.r,
                              ),
                            ),
                          ),
                          5.horizontalSpace,
                          state.trendingList[index].icon == null
                              ? Container()
                              : DefaultFadeImage(
                                  imageUrl: state.trendingList[index].icon!,
                                  width: 15.r,
                                  height: 15.r,
                                )
                        ],
                      ),
                    );
                  },
                  itemCount: state.trendingList.length,
                )
              : SizedBox(
                  height: 200.r,
                  child: Center(
                    child: SizedBox(
                      width: 30.r,
                      height: 30.r,
                      child:
                          CircularProgressIndicator(color: norMainThemeColors),
                    ),
                  ),
                )
        ],
      ),
    );
  }

  ///搜索结果
  Widget buildSearchResult(BuildContext context, state) {
    List<Widget> mainTabs = [];
    List<Widget> subTabs = [];
    List<SearchResultDataItem> items = state.searchResult.data.item!;

    mainTabs.add(Tab(text: SR.synthesis.tr, height: 30.r));
    for (var item in state.searchResult.data.nav) {
      String num = item.total > 99 ? "99+" : item.total.toString();
      item.total > 0
          ? mainTabs.add(Tab(text: "${item.name}($num)", height: 30.r))
          : mainTabs.add(Tab(text: item.name, height: 30.r));
    }
    subTabs.add(Tab(text: SR.defaultOrder.tr, height: 40.r));
    subTabs.add(Tab(text: SR.newestPublish.tr, height: 40.r));
    subTabs.add(Tab(text: SR.highVideoPlay.tr, height: 40.r));
    subTabs.add(Tab(text: SR.highDanmaku.tr, height: 40.r));

    ///头部的（综合番剧用户直播影视）分类；（默认排序新发布播放多弹幕多）子分类
    ///主分类
    ///次分类
    List<Widget> staggeredGridChildren = [
      DefaultTabController(
        length: 6,
        child: TabBar(
          onTap: (i) {
            clearFocus(context);
          },
          tabAlignment: TabAlignment.start,
          tabs: mainTabs,
          indicatorColor: norMainThemeColors,
          unselectedLabelColor: unselectedLabelColor,
          labelColor: norMainThemeColors,
          indicatorSize: TabBarIndicatorSize.label,
          labelStyle: TextStyle(
            fontSize: titleFontSizeCons.r,
            fontWeight: FontWeight.normal,
            fontFamily: bF,
          ),
          unselectedLabelStyle: TextStyle(
            fontSize: subTitleFontSizeCons.r,
            fontWeight: FontWeight.normal,
            fontFamily: bF,
          ),
          indicatorWeight: 2.r,
          isScrollable: true,
        ),
      ),
      DefaultTabController(
        length: 4,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 0.7.sw,
              child: TabBar(
                onTap: (i) {
                  clearFocus(context);
                },
                tabs: subTabs,
                indicator: const BoxDecoration(),
                indicatorColor: norMainThemeColors,
                unselectedLabelColor: unselectedLabelColor,
                labelColor: norMainThemeColors,
                indicatorSize: TabBarIndicatorSize.label,
                labelStyle: TextStyle(
                  fontSize: customFontSizeCons.r,
                  fontFamily: bF,
                  fontWeight: FontWeight.normal,
                ),
                unselectedLabelStyle: TextStyle(
                  fontSize: customFontSizeCons.r,
                  fontFamily: bF,
                  fontWeight: FontWeight.normal,
                ),
                labelPadding: EdgeInsets.zero,
              ),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: 1.r,
                  height: 15.r,
                  color: norGrayColor.withOpacity(.5),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.r),
                  child: Text(
                    SR.filter.tr,
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: norGrayColor,
                      fontSize: customFontSizeCons.r,
                    ),
                  ),
                ),
                Image.asset(
                  icFilterPNG,
                  width: customFontSizeCons.r,
                  height: customFontSizeCons.r,
                ),
              ],
            )
          ],
        ),
      ),
    ];

    ///搜索结果数据
    for (var item in items) {
      Widget child;
      if (item.linktype == "live") {
        ///直播类型的数据
        child = GestureDetector(
          onTap: () {
            // Navigator.of(context).pushNamed(HYLiveRoomPlayScreen.routeName,
            //     arguments: item.roomid!);
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 15.r),
            decoration: BoxDecoration(
                border: BorderDirectional(
                    bottom: BorderSide(color: norGrayColor.withOpacity(.1)))),
            child: Row(
              children: [
                ///封面
                Stack(
                  children: [
                    SizedBox(
                      width: 140.r,
                      height: 80.r,
                      child: ClipRRect(
                        borderRadius:
                            BorderRadius.all(const Radius.circular(4).r),
                        child: FadeInImage(
                          fit: BoxFit.fill,
                          placeholderFit: BoxFit.fill,
                          placeholder: AssetImage(icUpperVideoDefaultPNG),
                          image: NetworkImage(
                            item.cover,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      right: 5.r,
                      top: 5.r,
                      child: Container(
                        decoration: BoxDecoration(
                            color: norMainThemeColors,
                            borderRadius:
                                BorderRadius.all(const Radius.circular(3).r)),
                        padding: const EdgeInsets.symmetric(horizontal: 3).r,
                        child: Text(
                          item.badge!,
                          style: TextStyle(
                            fontSize: customFontSizeCons.r,
                            color: norWhite01Color,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                10.horizontalSpace,

                ///右侧视频信息
                Expanded(
                  child: SizedBox(
                    height: 80.r,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          item.title,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: norTextColors,
                            fontSize: customFontSizeCons.r,
                          ),
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Image.asset(
                                  upGrayPNG,
                                  width: customFontSizeCons.r,
                                  height: customFontSizeCons.r,
                                ),
                                5.horizontalSpace,
                                Text(
                                  item.author != null
                                      ? item.author!
                                      : item.name!,
                                  style: TextStyle(
                                    color: norGrayColor,
                                    fontSize: customFontSizeCons.r,
                                  ),
                                ),
                              ],
                            ),
                            4.verticalSpace,
                            item.online != null
                                ? Row(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Image.asset(
                                        viewGrayPNG,
                                        width: customFontSizeCons.r,
                                        height: customFontSizeCons.r,
                                      ),
                                      5.horizontalSpace,
                                      Text(
                                        changeToWan(item.online!),
                                        style: TextStyle(
                                          fontSize: customFontSizeCons.r,
                                          color: norGrayColor,
                                        ),
                                      ),
                                    ],
                                  )
                                : Container()
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      } else if (item.linktype == "channel") {
        ///频道类型的数据
        child = Container(
          padding: EdgeInsets.symmetric(vertical: 15.r),
          decoration: BoxDecoration(
              border: BorderDirectional(
                  bottom: BorderSide(color: norGrayColor.withOpacity(.1)))),
          child: Column(
            children: [
              Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.all(const Radius.circular(3).r),
                    child: DefaultFadeImage(
                      imageUrl: item.cover,
                      width: 43.r,
                      height: 43.r,
                    ),
                  ),
                  10.horizontalSpace,
                  Expanded(
                    child: SizedBox(
                      height: 43.r,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                item.title,
                                style: TextStyle(
                                  color: norBlackColors,
                                  fontSize: customFontSizeCons.r,
                                ),
                              ),
                              5.horizontalSpace,
                              DefaultFadeImage(
                                imageUrl: item.typeIcon!,
                                width: customFontSizeCons.r,
                                height: customFontSizeCons.r,
                              ),
                            ],
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                item.channelLabel1!.text,
                                style: TextStyle(
                                  color: norMainThemeColors,
                                  fontSize: customFontSizeCons.r,
                                ),
                              ),
                              10.horizontalSpace,
                              Text(
                                item.channelLabel2!.text,
                                style: TextStyle(
                                  color: norMainThemeColors,
                                  fontSize: customFontSizeCons.r,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 2.r, horizontal: 4.r),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(3.r)),
                        border:
                            Border.all(color: norMainThemeColors, width: 1.r)),
                    child: Text(
                      item.channelButton!.text,
                      style: TextStyle(
                        color: norMainThemeColors,
                        fontSize: customFontSizeCons.r,
                      ),
                    ),
                  ),
                  15.horizontalSpace,
                ],
              ),
              15.verticalSpace,
              item.items![0].goto! == "av"
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        buildChannelVideo(item, 0),
                        buildChannelVideo(item, 1),
                        buildChannelVideo(item, 2),
                      ],
                    )
                  : GridView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 70.r,
                        mainAxisSpacing: 10.r,
                      ),
                      itemBuilder: (ctx, i) {
                        return Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ClipRRect(
                              borderRadius:
                                  BorderRadius.all(const Radius.circular(3).r),
                              child: DefaultFadeImage(
                                imageUrl: item.items![i].cover!,
                                width: 35.r,
                                height: 35.r,
                              ),
                            ),
                            5.verticalSpace,
                            Text(
                              item.items![i].title!,
                              style: TextStyle(
                                fontSize: customFontSizeCons.r,
                              ),
                            ),
                          ],
                        );
                      },
                      itemCount: item.items!.length,
                    ),
            ],
          ),
        );
      } else if (item.linktype == "app_user") {
        ///用户
        child = Container(
          padding: EdgeInsets.symmetric(vertical: 15.r),
          decoration: BoxDecoration(
              border: BorderDirectional(
                  bottom: BorderSide(color: norGrayColor.withOpacity(.1)))),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: 4.r, top: 4.r, bottom: 4.r, right: 8.r),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(item.cover),
                      radius: 22.r,
                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              item.title,
                              style: TextStyle(
                                fontSize: customFontSizeCons.r,
                                fontWeight: FontWeight.bold,
                                color: norMainThemeColors,
                              ),
                            ),
                            4.horizontalSpace,
                            UserLevel(level: item.level!)
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              "粉丝:${changeToWan(item.fans!)}   视频:${changeToWan(item.archives!)}",
                              style: TextStyle(
                                fontSize: 12.sp,
                                color: norGrayColor,
                              ),
                            )
                          ],
                        ),
                        item.officialVerify!.desc != null
                            ? Text(
                                item.officialVerify!.desc!,
                                style: TextStyle(
                                  fontSize: customFontSizeCons.r,
                                  color: norGrayColor,
                                ),
                              )
                            : Container()
                      ],
                    ),
                  ),
                  Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 8.r, vertical: 2.r),
                    child: Text(
                      "+关注",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: customFontSizeCons.r,
                      ),
                    ),
                  ),
                  Image.asset(
                    videoMoreCustomPNG,
                    width: customFontSizeCons.r,
                    height: customFontSizeCons.r,
                  ),
                ],
              ),
              15.verticalSpace,

              ///前三个视频
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  buildAppUserVideo(item, 0),
                  buildAppUserVideo(item, 1),
                  buildAppUserVideo(item, 2),
                ],
              ),
              15.verticalSpace,
              Text(
                "查看全部${item.archives}个视频>",
                style: TextStyle(
                  color: norMainThemeColors,
                  fontSize: customFontSizeCons.r,
                ),
              ),
            ],
          ),
        );
      } else if (item.linktype == "esports") {
        ///赛事
        child = Container(
          padding: EdgeInsets.only(bottom: 15.r, left: 5.r, right: 5.r),
          decoration: BoxDecoration(
              border: BorderDirectional(
                  bottom: BorderSide(color: norGrayColor.withOpacity(.3)))),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                color: norBlue03Colors,
                width: 1.sw,
                height: 40.r,
                padding: EdgeInsets.only(
                  left: 15.r,
                  right: 10.r,
                  top: 4.r,
                  bottom: 4.r,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 4.r),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          DefaultFadeImage(
                            imageUrl: item.cover,
                            width: 40.r,
                            height: 40.r,
                            fit: BoxFit.cover,
                          ),
                          Text(
                            item.title,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: customFontSizeCons.r,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.transparent,
                          border: Border.all(color: Colors.white),
                          borderRadius:
                              BorderRadius.all(const Radius.circular(4).r)),
                      padding: EdgeInsets.symmetric(horizontal: 3.r),
                      child: Text(
                        "赛事专题>",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: customFontSizeCons.r,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              10.verticalSpace,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      15.verticalSpace,
                      DefaultFadeImage(
                        imageUrl: item.items![0].team1!.cover,
                        width: 45.r,
                        height: 45.r,
                      ),
                      25.verticalSpace,
                      Text(
                        item.items![0].team1!.title,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: customFontSizeCons.r,
                        ),
                      )
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        item.items![0].matchStage!,
                        style: TextStyle(
                          fontSize: customFontSizeCons.r,
                        ),
                      ),
                      10.verticalSpace,
                      Text(
                        "${item.items![0].team1!.score ?? "0"} : ${item.items![0].team2!.score ?? "0"}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: bigFontSizeCons.r,
                        ),
                      ),
                      5.verticalSpace,
                      item.items![0].matchLabel != null
                          ? Text(
                              item.items![0].matchLabel!.text,
                              style: TextStyle(
                                color: norMainThemeColors,
                                fontSize: customFontSizeCons.r,
                              ),
                            )
                          : Container(),
                      5.verticalSpace,
                      Container(
                        padding: EdgeInsets.symmetric(
                          vertical: 4.r,
                          horizontal: 8.r,
                        ),
                        decoration: BoxDecoration(
                          color: norMainThemeColors,
                          borderRadius: BorderRadius.all(Radius.circular(4.r)),
                        ),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Image.asset(
                              onlinePNG,
                              width: customFontSizeCons.r,
                              height: customFontSizeCons.r,
                            ),
                            4.horizontalSpace,
                            Text(
                              item.items![0].matchButton!.text,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: customFontSizeCons.r,
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      15.verticalSpace,
                      DefaultFadeImage(
                        imageUrl: item.items![0].team2!.cover,
                        width: 45.r,
                        height: 45.r,
                      ),
                      25.verticalSpace,
                      Text(
                        item.items![0].team2!.title,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: customFontSizeCons.r,
                        ),
                      )
                    ],
                  ),
                ],
              ),
              10.verticalSpace,
              Divider(
                color: norGrayColor.withOpacity(.3),
              ),
              10.verticalSpace,
              buildESportsRow(item, 1),
              15.verticalSpace,
              buildESportsRow(item, 2),
              20.verticalSpace,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  buildESportsBottomButton("全部赛事"),
                  buildESportsBottomButton("赛事视频"),
                  buildESportsBottomButton("征稿活动"),
                  buildESportsBottomButton("赛事话题"),
                ],
              ),
            ],
          ),
        );
      } else if (item.linktype == "video") {
        ///video视频类的数据
        child = GestureDetector(
          onTap: () {
            clearFocus(context);

            ///播放视频需要接受grpc型数据
            ///数据类型不符合，先放着
          },
          child: Container(
            padding: EdgeInsets.symmetric(
              vertical: 10.r,
              horizontal: 5.r,
            ),
            decoration: BoxDecoration(
              border: BorderDirectional(
                bottom: BorderSide(color: norGrayColor.withOpacity(.1)),
              ),
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                ///封面
                Stack(
                  children: [
                    SizedBox(
                      width: 140.r,
                      height: 80.r,
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(4.r)),
                        child: FadeInImage(
                          fit: BoxFit.fill,
                          placeholderFit: BoxFit.fill,
                          placeholder: AssetImage(icUpperVideoDefaultPNG),
                          image: NetworkImage(
                            item.cover,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      right: 5.r,
                      bottom: 5.r,
                      child: Container(
                        decoration: BoxDecoration(
                          color: norTextColors.withOpacity(.6),
                          borderRadius: BorderRadius.all(Radius.circular(3.r)),
                        ),
                        padding: EdgeInsets.symmetric(horizontal: 3.r),
                        child: Text(
                          item.duration!,
                          style: TextStyle(
                            fontSize: customFontSizeCons.r,
                            color: norWhite01Color,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                10.horizontalSpace,

                ///右侧视频信息
                Expanded(
                  child: SizedBox(
                    height: 88.r,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        HYHighlightStrInText(
                          originalText: item.title,
                          highlightText: state.searchKeywordText,
                          highlightColor: norMainThemeColors,
                          originalColor: norBlackColors,
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Image.asset(
                                  upGrayPNG,
                                  width: customFontSizeCons.r,
                                  height: customFontSizeCons.r,
                                ),
                                5.horizontalSpace,
                                Text(
                                  item.author!,
                                  style: TextStyle(
                                    color: norGrayColor,
                                    fontSize: customFontSizeCons.r,
                                  ),
                                ),
                              ],
                            ),
                            2.verticalSpace,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Image.asset(
                                      iconListPlayerPNG,
                                      width: customFontSizeCons.r,
                                      height: customFontSizeCons.r,
                                    ),
                                    5.horizontalSpace,
                                    Text(
                                      "${changeToWan(item.play!)}${item.showCardDesc2}",
                                      style: TextStyle(
                                        fontSize: customFontSizeCons.r,
                                        color: norGrayColor,
                                      ),
                                    ),
                                  ],
                                ),
                                Image.asset(
                                  videoMoreCustomPNG,
                                  width: customFontSizeCons.r,
                                  height: customFontSizeCons.r,
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      } else if (item.linktype == "bgm_media") {
        List<Widget> rowChildren = [];
        if (item.episodesNew!.length > 6) {
          for (int i = 0; i < 6; i++) {
            Widget child;
            if (i == 0 || i == 1) {
              child = buildNumberContainer(i.toString());
            } else if (i == 3) {
              child = buildNumberContainer(
                  (item.episodesNew!.length - 3).toString());
            } else if (i == 4) {
              child = buildNumberContainer(
                  (item.episodesNew!.length - 2).toString());
            } else if (i == 5) {
              child = buildNumberContainer(
                  (item.episodesNew!.length - 1).toString());
            } else {
              child = buildNumberContainer("...");
            }
            rowChildren.add(child);
          }
        } else {
          for (var i in item.episodesNew!) {
            rowChildren.add(buildNumberContainer(i.title));
          }
        }

        ///番剧
        child = Container(
          padding: EdgeInsets.symmetric(vertical: 15.r),
          decoration: BoxDecoration(
              border: BorderDirectional(
                  bottom: BorderSide(color: norGrayColor.withOpacity(.1)))),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(4.r)),
                    child: DefaultFadeImage(
                      imageUrl: item.cover,
                      width: 90.r,
                      height: 120.r,
                      fit: BoxFit.contain,
                    ),
                  ),
                  10.horizontalSpace,
                  Expanded(
                    child: SizedBox(
                      height: 120.r,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                item.title,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: titleFontSizeCons.r,
                                  fontWeight: FontWeight.bold,
                                  color: norBlackColors,
                                ),
                              ),
                              Text(
                                item.styles!,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: customFontSizeCons.r,
                                  color: norGrayColor,
                                ),
                              ),
                              Text(
                                item.style!,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: customFontSizeCons.r,
                                  color: norGrayColor,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 1.r,
                                    color: norMainThemeColors,
                                  ),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3.r)),
                                ),
                                padding: EdgeInsets.symmetric(
                                  horizontal: 6.r,
                                  vertical: 2.r,
                                ),
                                child: Text(
                                  item.watchButton!.title,
                                  style: TextStyle(
                                    fontSize: titleFontSizeCons.r,
                                    color: norMainThemeColors,
                                  ),
                                ),
                              ),
                              10.horizontalSpace,
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3.r)),
                                  color: norMainThemeColors,
                                ),
                                padding: EdgeInsets.symmetric(
                                  horizontal: 6.r,
                                  vertical: 2.r,
                                ),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    DefaultFadeImage(
                                      imageUrl: item.followButton!.icon,
                                      width: iconSizeCons.r,
                                      height: iconSizeCons.r,
                                    ),
                                    Text(
                                      item.followButton!.texts["0"]!,
                                      style: TextStyle(
                                        fontSize: iconSizeCons.r,
                                        color: norWhite01Color,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: item.rating.toString(),
                              style: TextStyle(
                                fontSize: iconSizeCons.r,
                                color: norYellow02Colors,
                              ),
                            ),
                            TextSpan(
                              text: SR.points.tr,
                              style: TextStyle(
                                fontSize: customFontSizeCons.r,
                                color: norYellow02Colors,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Text(
                        "${item.vote}${SR.people.tr}",
                        style: TextStyle(
                          fontSize: customFontSizeCons.r,
                          color: norGrayColor,
                        ),
                      ),
                    ],
                  )
                ],
              ),
              10.verticalSpace,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: rowChildren,
              ),
            ],
          ),
        );
      } else {
        return Container();
      }
      staggeredGridChildren.add(child);
    }
    return Expanded(
      child: EasyRefresh(
        child: StaggeredGrid.count(
          crossAxisCount: 1,
          children: staggeredGridChildren,
        ),
      ),
    );
  }

  Widget buildAppUserVideo(SearchResultDataItem item, int position) {
    return Container(
      width: 110.r,
      height: 120.r,
      decoration:
          BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(3.r))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ///封面
          Stack(
            children: [
              SizedBox(
                width: 110.r,
                height: 63.r,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(4.r)),
                  child: FadeInImage(
                    fit: BoxFit.fill,
                    placeholderFit: BoxFit.fill,
                    placeholder: AssetImage(icUpperVideoDefaultPNG),
                    image: NetworkImage(
                      item.avItems![position].cover,
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 5.r,
                bottom: 5.r,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.asset(
                      icPlayVideoWhitePNG,
                      width: iconSizeCons.r,
                      height: iconSizeCons.r,
                    ),
                    3.horizontalSpace,
                    Text(
                      changeToWan(item.avItems![position].play),
                      style: TextStyle(
                        fontSize: customFontSizeCons.r,
                        color: norWhite01Color,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),

          ///标题
          Container(
            padding: EdgeInsets.symmetric(horizontal: 4.r, vertical: 5.r),
            child: Text(
              item.avItems![position].title,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: norTextColors,
                fontSize: customFontSizeCons.r,
              ),
            ),
          ),

          ///发布时间
          Text(
            item.avItems![position].ctimeLabel,
            style: TextStyle(
              color: norGrayColor,
              fontSize: customFontSizeCons.r,
            ),
          )
        ],
      ),
    );
  }

  Widget buildChannelVideo(SearchResultDataItem item, int position) {
    return Container(
      width: 108.r,
      height: 115.r,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(const Radius.circular(3).r)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ///封面
          Stack(
            children: [
              SizedBox(
                width: 108.r,
                height: 70.r,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(const Radius.circular(4).r),
                  child: FadeInImage(
                    fit: BoxFit.fill,
                    placeholderFit: BoxFit.fill,
                    placeholder: AssetImage(icUpperVideoDefaultPNG),
                    image: NetworkImage(
                      item.items![position].cover!,
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 5.r,
                bottom: 5.r,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.asset(
                      icPlayVideoWhitePNG,
                      width: iconSizeCons.r,
                      height: iconSizeCons.r,
                    ),
                    3.horizontalSpace,
                    Text(
                      item.items![position].coverLeftText1!,
                      style: TextStyle(
                        fontSize: customFontSizeCons.r,
                        color: norWhite01Color,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),

          ///标题
          Container(
            padding: EdgeInsets.symmetric(horizontal: 4.r, vertical: 5.r),
            child: Text(
              item.items![position].title!,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: norTextColors,
                fontSize: customFontSizeCons.r,
              ),
            ),
          ),
        ],
      ),
    );
  }

  ///第2,3条赛事
  Widget buildESportsRow(SearchResultDataItem item, int index) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text(
          item.items![index].matchTime!.text,
          style: TextStyle(
            fontSize: customFontSizeCons.r,
          ),
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              noMoreN(item.items![index].team1!.title, 5),
              style: TextStyle(
                fontSize: customFontSizeCons.r,
              ),
            ),
            20.horizontalSpace,
            Text(
              "VS",
              style: TextStyle(
                fontSize: customFontSizeCons.r,
                color: norGrayColor,
              ),
            ),
            20.horizontalSpace,
            Text(
              noMoreN(item.items![index].team2!.title, 5),
              style: TextStyle(
                fontSize: customFontSizeCons.r,
              ),
            ),
          ],
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.r, vertical: 3.r),
          decoration: BoxDecoration(
              border: Border.all(
                color: norMainThemeColors,
              ),
              color: norWhite01Color,
              borderRadius: BorderRadius.all(Radius.circular(4.r))),
          child: Text(
            item.items![index].matchButton!.text,
            style: TextStyle(
              color: norMainThemeColors,
              fontSize: customFontSizeCons.r,
            ),
          ),
        ),
      ],
    );
  }

  ///全部赛事、赛事视频、征稿活动、赛事话题
  Widget buildESportsBottomButton(String title) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15.r, vertical: 4.r),
      decoration: BoxDecoration(
          color: norWhite01Color,
          border: Border.all(color: norGrayColor.withOpacity(.3))),
      child: Text(
        title,
        style: TextStyle(
          color: norTextColors,
          fontSize: customFontSizeCons.r,
        ),
      ),
    );
  }

  Widget buildNumberContainer(String num) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(4.r)),
          border: Border.all(
            color: norGrayColor.withOpacity(.15),
          )),
      alignment: Alignment.center,
      width: 45.r,
      height: 45.r,
      child: Text(
        num,
        style: TextStyle(
          color: norTextColors,
        ),
      ),
    );
  }
}
