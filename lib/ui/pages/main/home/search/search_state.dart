import 'package:flutter/material.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

import '../../../../../core/model/other/home_search_result.dart';
import '../../../../../core/model/other/search_keywords_model.dart';

class SearchState {
  late HYSearchKeywordModel searchKeyword;
  late List<ListElement> trendingList;
  late TextEditingController textEditingController;
  late FocusNode focusNode;
  late HYSearchResultModel searchResult;
  late bool showResult;

  ///当前搜索的文本
  late String searchKeywordText;

  /// 正在输入的文本
  late RxString isInputKeyword;

  SearchState() {
    trendingList = [];
    textEditingController = TextEditingController();
    showResult = false;
    focusNode = FocusNode();
    searchKeywordText = "";
    isInputKeyword = "".obs;
  }
}
