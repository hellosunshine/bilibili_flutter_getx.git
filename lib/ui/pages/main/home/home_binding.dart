import 'package:bilibili_getx/ui/pages/main/home/comic/comic_logic.dart';
import 'package:bilibili_getx/ui/pages/main/home/home_logic.dart';
import 'package:bilibili_getx/ui/pages/main/home/live/live_logic.dart';
import 'package:get/get.dart';

import 'recommend/recommend_logic.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomeLogic());
    Get.lazyPut(() => ComicLogic());
    Get.lazyPut(() => RecommendLogic());
    Get.lazyPut(() => LiveLogic());
  }
}
