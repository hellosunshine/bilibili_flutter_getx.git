import 'package:get/get.dart';

import 'animation_study_state.dart';

class AnimationStudyLogic extends GetxController {
  final AnimationStudyState state = AnimationStudyState();
}
