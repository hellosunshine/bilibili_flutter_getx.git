import 'package:bilibili_getx/core/constant_util/app_theme.dart';
import 'package:bilibili_getx/ui/shared/image_asset.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'we_chat_setting_logic.dart';

class WeChatSettingView extends StatelessWidget with HYAppTheme, ImageAssets {
  static String routeName = "/wechat_setting";
  final logic = Get.find<WeChatSettingLogic>();
  final state = Get.find<WeChatSettingLogic>().state;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<WeChatSettingLogic>(
      builder: (logic) {
        return Scaffold(
          appBar: AppBar(
            elevation: .1,
            backgroundColor: norWhite09Color,
            centerTitle: true,
            leading: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Icon(
                Icons.chevron_left,
                color: norBlackColors,
              ),
            ),
            title: Text(
              "设置",
              style: TextStyle(
                fontSize: 13.sp,
                color: norBlackColors,
              ),
            ),
          ),
          body: state.textButtons.isNotEmpty
              ? ListView.separated(
                  padding: EdgeInsets.only(bottom: 40.r),
                  physics: BouncingScrollPhysics(),
                  itemCount: state.textButtons.length,
                  itemBuilder: (ctx, index) {
                    if (index == state.textButtons.length - 2 ||
                        index == state.textButtons.length - 1) {
                      return GestureDetector(
                        onTap: () {
                          state.textButtons[index].onTapContext!(context);
                        },
                        child: Container(
                          color: norWhite01Color,
                          padding: EdgeInsets.symmetric(
                            vertical: 10.r,
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            state.textButtons[index].text,
                            style: TextStyle(
                              fontSize: 18.sp,
                              color: norBlackColors,
                            ),
                          ),
                        ),
                      );
                    } else {
                      return Container(
                        padding: EdgeInsets.symmetric(
                          vertical: 10.r,
                          horizontal: 15.r,
                        ),
                        color: norWhite01Color,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              state.textButtons[index].text,
                              style: TextStyle(
                                fontSize: 18.sp,
                                color: norBlackColors,
                              ),
                            ),
                            Icon(
                              Icons.keyboard_arrow_right,
                              color: norGrayColor,
                            ),
                          ],
                        ),
                      );
                    }
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    if (index == 0 ||
                        index == 2 ||
                        index == 10 ||
                        index == 11 ||
                        index == 13 ||
                        index == 14 ||
                        index == 15) {
                      return 8.verticalSpace;
                    } else if (index == 6) {
                      return Container(
                        padding: EdgeInsets.only(
                            top: 20.r, left: 10.r, bottom: 10.r),
                        width: 1.sw,
                        child: Text(
                          "隐私",
                          style: TextStyle(
                            fontSize: 13.sp,
                            color: norGrayColor,
                          ),
                        ),
                      );
                    } else {
                      return Container(
                        height: .5.r,
                        width: 1.sw,
                        color: norGrayColor,
                      );
                    }
                  },
                )
              : Container(),
        );
      },
    );
  }
}
