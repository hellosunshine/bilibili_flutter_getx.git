import 'package:get/get.dart';

import '../chat_room/chat_room_logic.dart';
import 'my_we_chat_logic.dart';
import 'wechat_main/wechat_main_logic.dart';
import 'wechat_mine/wechat_mine_logic.dart';

class MyWeChatBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => MyWeChatLogic());
    Get.lazyPut(() => ChatRoomLogic());
    Get.lazyPut(() => WechatMainLogic());
    Get.lazyPut(() => WechatMineLogic());
  }
}
