import 'package:bilibili_getx/core/model/wechat/text_button_model.dart';
import 'package:bilibili_getx/core/model/other/wechat_login_model.dart';

class WechatMineState {
  late WechatLoginData wechatLoginData;
  late List<TextButtonModel> textButtons;

  WechatMineState() {
    textButtons = [];
  }
}
