import 'package:get/get.dart';
import '../../wechat_main/wechat_main_logic.dart';
import 'we_chat_setting_logic.dart';

class MyWeChatSettingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => WeChatSettingLogic());
    Get.lazyPut(() => WechatMainLogic());
  }
}
