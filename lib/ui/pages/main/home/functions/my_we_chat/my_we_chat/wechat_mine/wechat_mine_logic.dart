import 'package:bilibili_getx/core/model/wechat/text_button_model.dart';
import 'package:bilibili_getx/ui/pages/main/home/functions/my_we_chat/my_we_chat/wechat_mine/we_chat_setting/we_chat_setting_view.dart';
import 'package:bilibili_getx/ui/shared/image_asset.dart';
import 'package:get/get.dart';

import 'wechat_mine_state.dart';

class WechatMineLogic extends GetxController with ImageAssets {
  final WechatMineState state = WechatMineState();

  initWechatMine() {
    state.textButtons = [
      TextButtonModel(
        text: "服务",
        icon: wechatServicePng,
        onTap: () {},
      ),
      TextButtonModel(
        text: "收藏",
        icon: wechatCollectionPng,
        onTap: () {},
      ),
      TextButtonModel(
        text: "朋友圈",
        icon: wechatFriendCirclePng,
        onTap: () {},
      ),
      TextButtonModel(
        text: "卡包",
        icon: wechatPackagePng,
        onTap: () {},
      ),
      TextButtonModel(
        text: "表情",
        icon: wechatEmojiPng,
        onTap: () {},
      ),
      TextButtonModel(
        text: "设置",
        icon: wechatSettingPng,
        onTap: () {
          Get.toNamed(WeChatSettingView.routeName);
        },
      ),
    ];
  }
}
