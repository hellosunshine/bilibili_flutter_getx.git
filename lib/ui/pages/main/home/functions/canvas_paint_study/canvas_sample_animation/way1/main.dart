import 'package:bilibili_getx/ui/pages/main/home/functions/canvas_paint_study/canvas_sample_animation/way1/pic_man.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(child: PicMan()),
    );
  }
}
