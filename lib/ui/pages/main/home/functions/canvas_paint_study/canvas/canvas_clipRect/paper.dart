import 'dart:ui';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';

import '../../mix/grid.dart';

class Paper extends StatelessWidget {
  const Paper({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      // width: 100,
      // height: 100,
      color: Colors.white,
      child: CustomPaint(
        size: Size(50, 50),
        painter: PaperCustomPainter(),
      ),
    );
  }
}

class PaperCustomPainter extends CustomPainter with Grid {
  final Paint _paint = Paint()
    ..style = PaintingStyle.fill
    ..color = Colors.black;

  @override
  void paint(Canvas canvas, Size size) {
    drawGrid(canvas, size);
    drawAxis(canvas, size);
    var rect = Rect.fromCenter(center: Offset.zero, width: 360, height: 240);
    canvas.clipRect(rect, doAntiAlias: true, clipOp: ClipOp.intersect);
    var colors = [
      const Color(0xFFF60C0C),
      const Color(0xFFF3B913),
      const Color(0xFFE7F716),
      const Color(0xFF3DF30B),
      const Color(0xFF0DF6EF),
      const Color(0xFF0829FB),
      const Color(0xFFB709F4),
    ];
    var pos = [1.0 / 7, 2.0 / 7, 3.0 / 7, 4.0 / 7, 5.0 / 7, 6.0 / 7, 1.0];
    _paint.shader = ui.Gradient.linear(
        rect.center, rect.centerRight, colors, pos, TileMode.clamp);
    _paint.blendMode = BlendMode.lighten;
    canvas.drawRect(rect, _paint);
  }

  @override
  bool shouldRepaint(covariant PaperCustomPainter oldDelegate) => false;
}
