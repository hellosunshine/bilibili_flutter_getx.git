import 'package:flutter/cupertino.dart';

import '../../../../../core/model/other/feed_index_model.dart';

class RecommendState {
  late List<Widget> pageWidget;
  late List<List<FeedIndexItem>> pageData;

  RecommendState() {
    pageWidget = [];
    pageData = [];
  }
}
