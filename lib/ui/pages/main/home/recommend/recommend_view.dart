import 'package:bilibili_getx/core/constant_util/size.dart';
import 'package:bilibili_getx/core/constant_util/app_theme.dart';
import 'package:bilibili_getx/ui/widgets/custom/auto_keep_alive_wrapper.dart';
import 'package:bilibili_getx/ui/widgets/custom/bilibili_classical_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../../core/I18n/str_res_keys.dart';
import '../../../../shared/image_asset.dart';
import 'recommend_logic.dart';

class HomeRecommendView extends StatelessWidget
    with SizeConstantUtil, ImageAssets, HYAppTheme {
  final logic = Get.find<RecommendLogic>();
  final state = Get.find<RecommendLogic>().state;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RecommendLogic>(
      builder: (logic) {
        logic.initHomeRecommendWidgets(context);
        return state.pageData.isEmpty
            ? Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.asset(
                      holderLoadingPNG,
                      width: 150.r,
                      height: 150.r,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10.r),
                      child: TextButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(norMainThemeColors),
                        ),
                        onPressed: () {
                          ///有网络时拉取数据
                          logic.refreshData();
                        },
                        child: Text(
                          SR.refresh.tr,
                          style: TextStyle(
                            color: norWhite01Color,
                            fontSize: customFontSizeCons.r,
                            fontWeight: FontWeight.normal,
                            fontFamily: bF,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            : AutoKeepAliveWrapper(
                child: EasyRefresh(
                  header: BilibiliClassicalHeader(
                    enableHapticFeedback: false,
                    float: true,
                  ),
                  footer: ClassicalFooter(
                    showInfo: false,
                  ),
                  onRefresh: () async {
                    await logic.refreshAddData(context);
                  },
                  onLoad: () async {
                    logic.loadAddData(context);
                  },
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: 6.r,
                      right: 6.r,
                      top: 4.r,
                      bottom: 0.r,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: state.pageWidget,
                    ),
                  ),
                ),
              );
      },
    );
  }
}
