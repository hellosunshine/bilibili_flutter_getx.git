
import 'package:bilibili_getx/core/constant_util/size.dart';
import 'package:bilibili_getx/core/responsive_layout/extension_context_responsive/extension_context.dart';
import 'package:bilibili_getx/core/service/utils/constant.dart';
import 'package:bilibili_getx/ui/shared/image_asset.dart';
import 'package:bilibili_getx/ui/widgets/custom/fade_image_default.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper_null_safety_flutter3/flutter_swiper_null_safety_flutter3.dart';
import 'package:get/get.dart';

import '../../../../../core/model/other/xliveAppInterfaceV2IndexFeedModel.dart';
import '../../../../../core/constant_util/app_theme.dart';
import 'live_logic.dart';

class LiveScreen extends StatefulWidget {
  static const String routeName = "/home/live";

  const LiveScreen({super.key});

  @override
  State<LiveScreen> createState() => LiveScreenState();
}

class LiveScreenState extends State<LiveScreen>
    with
        AutomaticKeepAliveClientMixin,
        SizeConstantUtil,
        HYAppTheme,
        ImageAssets {
  final logic = Get.find<LiveLogic>();
  final state = Get.find<LiveLogic>().liveState;

  @override
  void initState() {
    logic.initAndroidXliveAppInterfaceV2IndexFeedData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GetBuilder<LiveLogic>(
      builder: (logic) {
        if (state.isLoadingLiveData == false) {
          return initAndroidLiveView();
        } else {
          return Container(
            margin: EdgeInsets.only(top: 30.r),
            alignment: Alignment.topCenter,
            width: 1.sw,
            child: RefreshProgressIndicator(
              value: null,
              color: norMainThemeColors,
            ),
          );
        }
      },
    );
  }

  Widget initWebLiveView() {
    return const Text("data");
  }

  ///初始化Android界面
  Widget initAndroidLiveView() {
    for (var item in state.cardList) {
      if (item.cardType == "banner_v1") {
        state.cardDataBannerV1 = item.cardData;
      } else if (item.cardType == "area_entrance_v3") {
        state.cardDataAreaEntranceV3 = item.cardData;
      } else if (item.cardType == "activity_card_v1") {
        // state.cardDataActivityCardV1 = item.cardData;
      } else if (item.cardType == "small_card_v1") {
        state.cardDataSmallCardV1.add(item.cardData);
      } else {
        if (Constant.isDebug) {
          if (kDebugMode) {
            print("不存在${item.cardType}");
          }
        }
      }
    }
    return Padding(
      padding: EdgeInsets.all(4.r),
      child: CustomScrollView(
        // controller: state.customScrollViewScrollController,
        shrinkWrap: true,
        // physics: const NeverScrollableScrollPhysics(),
        slivers: [
          SliverToBoxAdapter(
            child: buildBannerV1(),
          ),
          SliverAppBar(
            elevation: 0,
            backgroundColor: norWhite01Color,
            pinned: true,
            leading: null,
            automaticallyImplyLeading: false,
            title: buildAreaEntranceV3(state.cardDataAreaEntranceV3),
            actions: [
              Container(
                margin: EdgeInsets.only(right: 10.r),
                child: Image.asset(
                  moreBlockPNG,
                  width: iconSizeCons.r,
                  height: iconSizeCons.r,
                ),
              ),
            ],
          ),
          SliverGrid(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return buildSmallCardV1(state.cardDataSmallCardV1[index]);
              },
              childCount: state.cardDataSmallCardV1.length,
            ),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: context.responsive<int>(
                2,
                sm: 2,
                md: 3,
                lg: 4,
                xl: 5,
              ),
              mainAxisSpacing: 6.r,
              crossAxisSpacing: 6.r,
              mainAxisExtent: mainAxisExtent.r,
            ),
          )
        ],
      ),
    );
  }

  ///初始化广告
  Widget buildBannerV1() {
    return Container(
      color: norWhite01Color,
      height: state.headerHeight,
      width: 1.sw,
      child: Swiper(
        autoplay: state.isAutoPlay,
        pagination: SwiperPagination(
          margin: EdgeInsets.only(bottom: 3.r, right: 3.r),
          alignment: Alignment.bottomRight,
          builder: DotSwiperPaginationBuilder(
            activeColor: norMainThemeColors,
            size: unSelectFontSizeCons.r,
            activeSize: selectFontSizeCons.r,
            color: norWhite01Color,
          ),
        ),
        itemBuilder: (ctx, index) {
          return ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(6.r)),
            child: DefaultFadeImage(
              imageUrl: state.cardDataBannerV1.bannerV1!.list[index].pic,
              fit: BoxFit.cover,
            ),
          );
        },
        itemCount: state.cardList[0].cardData.bannerV1!.list.length,
      ),
    );
  }

  ///导航栏
  Widget buildAreaEntranceV3(CardData cardData) {
    List<Tab> tabs = [const Tab(text: '推荐')];
    for (var item in cardData.areaEntranceV3!.list) {
      tabs.add(Tab(
        text: item.title,
      ));
    }
    return DefaultTabController(
      // key: state.liveTabBarGlobalKey,
      length: tabs.length,
      child: TabBar(
        tabAlignment: TabAlignment.start,
        onTap: (index) {
          logic.selectLabelAndFetchData(index);
        },
        indicatorColor: Colors.transparent,
        unselectedLabelColor: norGrayColor,
        labelColor: norTextColors,
        labelPadding: EdgeInsets.symmetric(horizontal: 8.r),
        labelStyle: TextStyle(
          fontSize: selectTabFontSizeCons.r,
          fontWeight: FontWeight.normal,
          fontFamily: 'bilibiliFonts',
        ),
        unselectedLabelStyle: TextStyle(
          fontSize: unSelectTabFontSizeCons.r,
          fontWeight: FontWeight.normal,
          fontFamily: 'bilibiliFonts',
        ),
        isScrollable: true,
        tabs: tabs,
      ),
    );
  }

  Widget buildSmallCardV1(CardData item) {
    return GestureDetector(
      onTap: () {
        // print(item.smallCardV1!.id);
        ///跳转至直播间
        logic.go2LivePlay(item);
      },
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(5.r)),
        elevation: 1,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(5.r)),
                  child: DefaultFadeImage(
                    imageUrl: item.smallCardV1!.cover,
                    height: coverHeight.r,
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    width: 1.sw,
                    height: 25.r,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                      colors: [
                        Colors.transparent,
                        Colors.black.withOpacity(.6),
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    )),
                  ),
                ),
                Positioned(
                  left: 8.r,
                  bottom: 5.r,
                  child: SizedBox(
                    height: customFontSizeCons.r,
                    child: Text(
                      item.smallCardV1!.coverLeftStyle.text,
                      style: TextStyle(
                        color: norWhite01Color,
                        fontSize: customFontSizeCons.r,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  right: 8.r,
                  bottom: 5.r,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        height: customFontSizeCons.r,
                        child: Image.asset(
                          seenPNG,
                          height: customFontSizeCons.r,
                          width: customFontSizeCons.r,
                        ),
                      ),
                      5.horizontalSpace,
                      Container(
                        alignment: Alignment.center,
                        height: customFontSizeCons.r,
                        child: Text(
                          item.smallCardV1!.coverRightStyle.text,
                          style: TextStyle(
                            color: norWhite01Color,
                            fontSize: customFontSizeCons.r,
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 8.r, horizontal: 4.r),
              width: 1.sw,
              child: Text(
                item.smallCardV1!.title,
                style: TextStyle(
                  color: norTextColors,
                  fontSize: titleFontSizeCons.r,
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 4.r),
              width: 1.sw,
              child: Text(
                item.smallCardV1!.subtitleStyle.text,
                style: TextStyle(
                  color: norGrayColor,
                  fontSize: customFontSizeCons.r,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
