import 'package:bilibili_getx/core/I18n/str_res_keys.dart';
import 'package:bilibili_getx/core/constant_util/app_theme.dart';
import 'package:bilibili_getx/core/constant_util/size.dart';
import 'package:bilibili_getx/ui/shared/image_asset.dart';
import 'package:bilibili_getx/ui/widgets/custom/round_underline_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'local_image/local_image_view.dart';
import 'local_video/local_video_view.dart';
import 'upload_logic.dart';

class UploadView extends StatefulWidget {
  @override
  State<UploadView> createState() => _UploadViewState();
}

class _UploadViewState extends State<UploadView>
    with TickerProviderStateMixin, HYAppTheme, ImageAssets, SizeConstantUtil {
  final logic = Get.find<UploadLogic>();
  final state = Get.find<UploadLogic>().state;

  @override
  void initState() {
    ///监听TabBar和TabBarView，两个都要加，一个是点击，一个是左右滑动监听
    state.mainTabController = TabController(vsync: this, length: 2);
    state.mainTabController.addListener(() {
      // logic.updateTabName();
    });
    state.subTabController = TabController(vsync: this, length: 3);
    state.subTabController.addListener(() {
      // logic.updateTabName();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<UploadLogic>(builder: (logic) {
      return CustomScrollView(
        slivers: [
          SliverAppBar(
            leading: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Icon(
                Icons.clear,
                color: norWhite01Color,
                size: iconSizeCons,
              ),
            ),
            backgroundColor: Colors.transparent,
            pinned: true,
            title: buildUpLoadMainTabBar(),
            // centerTitle: true,
            automaticallyImplyLeading: false,
          ),
          SliverFillRemaining(
            child: TabBarView(
              controller: state.mainTabController,
              children: [
                buildRecentlyFiles(),
                buildScript(),
              ],
            ),
          ),
        ],
      );
    });
  }

  Widget buildScript() {
    return const Text("草稿");
  }

  Widget buildRecentlyFiles() {
    return CustomScrollView(
      slivers: [
        SliverAppBar(
          leading: null,
          automaticallyImplyLeading: false,
          backgroundColor: Colors.transparent,
          pinned: true,
          title: buildUpLoadSubTabBar(),
          // centerTitle: true,
        ),
        SliverFillRemaining(
          child: TabBarView(
            controller: state.subTabController,
            children: [
              const Text("全部"),
              LocalVideoComponent(),
              LocalImageComponent()
            ],
          ),
        )
      ],
    );
  }

  ///顶部主TabBar
  Widget buildUpLoadMainTabBar() {
    return DefaultTabController(
      length: 2,
      child: TabBar(
        controller: state.mainTabController,
        labelColor: norWhite01Color,
        indicator: BilibiliRoundUnderlineTabIndicator(
          insets: EdgeInsets.only(
            left: 50.r,
            right: 50.r,
            bottom: 5.r,
          ),
          borderSide: BorderSide(
            width: 4.r,
            color: norWhite01Color,
          ),
        ),
        indicatorSize: TabBarIndicatorSize.tab,
        labelStyle: TextStyle(
          color: norWhite01Color,
          fontWeight: FontWeight.normal,
          fontSize: titleFontSizeCons.r,
          fontFamily: bF,
        ),
        unselectedLabelStyle: TextStyle(
          color: norWhite01Color,
          fontWeight: FontWeight.normal,
          fontSize: titleFontSizeCons.r,
          fontFamily: bF,
        ),
        tabs: [
          Tab(
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: "${SR.recentlyProject.tr} ",
                    style: TextStyle(
                      color: norWhite01Color,
                      fontSize: titleFontSizeCons.r,
                      fontFamily: bF,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  WidgetSpan(
                    alignment: PlaceholderAlignment.bottom,
                    child: Image.asset(
                      expandedDarkPNG,
                      height: titleFontSizeCons.r,
                      width: titleFontSizeCons.r,
                    ),
                  )
                ],
              ),
            ),
          ),
          Tab(text: SR.script.tr)
        ],
      ),
    );
  }

  Widget buildUpLoadSubTabBar() {
    return TabBar(
      controller: state.subTabController,
      labelColor: norWhite01Color,
      indicator: BilibiliRoundUnderlineTabIndicator(
        insets: EdgeInsets.only(
          left: 50.r,
          right: 50.r,
          bottom: 5.r,
        ),
        borderSide: BorderSide(
          width: 3.r,
          color: norWhite01Color.withOpacity(.5),
        ),
      ),
      indicatorSize: TabBarIndicatorSize.tab,
      labelStyle: TextStyle(
        color: norWhite01Color,
        fontWeight: FontWeight.normal,
        fontSize: subTitleFontSizeCons.r,
        fontFamily: bF,
      ),
      unselectedLabelStyle: TextStyle(
        color: norWhite01Color,
        fontWeight: FontWeight.normal,
        fontSize: subTitleFontSizeCons.r,
        fontFamily: bF,
      ),
      tabs: [
        Tab(text: SR.all.tr),
        Tab(text: SR.video.tr),
        Tab(text: SR.photo.tr),
      ],
    );
  }

// Widget buildUploadFilePreview() {
//   if(state.fileType == 0) {
//     ///视频文件
//     return Container();
//   } else if(state.fileType == 1){
//     ///图片文件
//     return Image.file(
//       File(state.fileSrc),
//       fit: BoxFit.contain,
//     );
//   } else {
//     ///文件类型
//     return Center(
//       child: Text("文件类型"),
//     );
//   }
// }
//
// ///加载手机内文件夹内容
// Widget buildUploadFiles() {
//   return const Center(
//     child: Text(
//       "文件夹，待写",
//       style: TextStyle(color: Colors.white),
//     ),
//   );
// }
//
//
// ///编辑视频、草稿箱
// List<Widget> buildUpLoadActions() {
//   return [
//     Container(
//       alignment: Alignment.center,
//       padding: const EdgeInsets.only(right: 20).r,
//       decoration: BoxDecoration(
//           borderRadius: BorderRadius.all(Radius.circular(30.r))),
//       child: Text(
//         SR.editVideo.tr,
//         style: TextStyle(
//           color: norWhite01Color,
//           fontSize: 14.sp,
//         ),
//       ),
//     ),
//     Container(
//       alignment: Alignment.center,
//       padding: const EdgeInsets.only(right: 20).r,
//       decoration: BoxDecoration(
//           borderRadius: BorderRadius.all(Radius.circular(30.r))),
//       child: Text(
//         SR.script.tr,
//         style: TextStyle(
//           color: norWhite01Color,
//           fontSize: 14.sp,
//         ),
//       ),
//     )
//   ];
// }
}
