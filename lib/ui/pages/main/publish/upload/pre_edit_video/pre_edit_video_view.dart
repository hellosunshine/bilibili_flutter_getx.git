import 'package:bilibili_getx/core/I18n/str_res_keys.dart';
import 'package:bilibili_getx/core/constant_util/app_theme.dart';
import 'package:bilibili_getx/ui/pages/video_play/bilibili_video_player/bilibili_video_player_view.dart';
import 'package:bilibili_getx/ui/shared/image_asset.dart';
import 'package:bilibili_getx/ui/widgets/custom/custom_floating_action_button_location.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../pre_publish_video/pre_publish_video_view.dart';
import 'pre_edit_video_logic.dart';

class PreEditVideoScreen extends StatefulWidget {
  static String routeName = "/pre_edit_video";

  const PreEditVideoScreen({super.key});

  @override
  State<PreEditVideoScreen> createState() => _PreEditVideoScreenState();
}

class _PreEditVideoScreenState extends State<PreEditVideoScreen>
    with HYAppTheme, ImageAssets {
  final logic = Get.find<PreEditVideoLogic>();
  final state = Get.find<PreEditVideoLogic>().state;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        // appBar: AppBar(
        //   elevation: .1,
        //   title: Text(
        //     "编辑视频",
        //     style: TextStyle(
        //         fontSize: 14.sp,
        //         color: norWhite01Color,
        //         fontWeight: FontWeight.normal,
        //         fontFamily: 'bilibiliFonts'),
        //   ),
        //   backgroundColor: norTextColors,
        //   leading: GestureDetector(
        //     onTap: () {
        //       Get.back();
        //     },
        //     child: const Icon(
        //       Icons.arrow_back,
        //       color: norWhite01Color,
        //     ),
        //   ),
        // ),
        body: BilibiliVideoPlayer(),

        ///压扁的floatingActionButton
        floatingActionButton: FloatingActionButton.extended(
          backgroundColor: norMainThemeColors,
          onPressed: () {
            Get.toNamed(PrePublishVideoScreen.routeName);
          },
          label: Text(
            SR.go2Publish.tr,
            style: TextStyle(
              fontSize: 14.sp,
              color: norWhite01Color,
              fontWeight: FontWeight.normal,
            ),
          ),
        ),
        floatingActionButtonLocation: CustomFloatingActionButtonLocation(
            FloatingActionButtonLocation.endFloat, -5, -70),
      ),
    );
  }
}
