import 'package:bilibili_getx/core/I18n/str_res_keys.dart';
import 'package:bilibili_getx/core/constant_util/app_theme.dart';
import 'package:bilibili_getx/core/constant_util/size.dart';
import 'package:bilibili_getx/core/platform_design_rules/platform_design_rules.dart';
import 'package:bilibili_getx/ui/pages/main/publish/upload/upload_view.dart';
import 'package:bilibili_getx/ui/widgets/custom/round_underline_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class PublishView extends StatelessWidget with HYAppTheme, SizeConstantUtil {
  static const String routeName = "/publish";

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        colorScheme: ColorScheme.light(
          primary: norMainThemeColors,
          surface: norBlackColors,
        ),
      ),
      child: DefaultTabController(
        initialIndex: 2,
        length: 5,
        child: Scaffold(
          appBar: AppBar(
            systemOverlayStyle: blackTheme,
            toolbarHeight: 0,
            elevation: 0,
          ),
          backgroundColor: norTextColors,
          bottomNavigationBar: SizedBox(
            height: DesignRules.getInstance().getBottomNavigationBarHeight(),
            child: TabBar(
              tabAlignment: TabAlignment.center,
              isScrollable: true,
              labelColor: norWhite01Color,
              tabs: [
                Tab(text: SR.createLiveRoom.tr),
                Tab(text: SR.photograph.tr),
                Tab(text: SR.upload.tr),
                Tab(text: SR.shareDynamic.tr),
                Tab(text: SR.templateAuthoring.tr),
              ],
              indicator: BilibiliRoundUnderlineTabIndicator(
                insets: EdgeInsets.symmetric(horizontal: 3.r),
                borderSide: BorderSide(
                  width: 5.r,
                  color: norMainThemeColors,
                ),
              ),
              indicatorSize: TabBarIndicatorSize.label,
              padding: EdgeInsets.only(bottom: 15.r),
              labelPadding: EdgeInsets.symmetric(horizontal: 20.r),
              labelStyle: TextStyle(
                color: norWhite01Color,
                fontWeight: FontWeight.normal,
                fontSize: titleFontSizeCons.r,
                fontFamily: bF,
              ),
              unselectedLabelStyle: TextStyle(
                color: norWhite01Color,
                fontWeight: FontWeight.normal,
                fontSize: subTitleFontSizeCons.r,
                fontFamily: bF,
              ),
            ),
          ),
          body: buildPublishView(),
        ),
      ),
    );
  }

  Widget buildPublishView() {
    return TabBarView(
      children: [
        Container(),
        Container(),
        UploadView(),
        Container(),
        Container(),
      ],
    );
  }
}
