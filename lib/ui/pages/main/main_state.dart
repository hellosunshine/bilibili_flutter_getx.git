import 'package:bilibili_getx/ui/pages/main/dynamic_circle/dynamic_circle_view.dart';
import 'package:bilibili_getx/ui/pages/main/mall/mall_view.dart';
import 'package:bilibili_getx/ui/pages/main/mine/mine_view.dart';
import 'package:flutter/cupertino.dart';

import 'home/home_view.dart';
import 'publish/publish_view.dart';

class MainState {
  ///当前下标
  late int currentIndex;

  ///主页界面子页面
  late List<Widget> mainPages;

  MainState() {
    currentIndex = 0;
    mainPages = [
      HomeScreen(),
      DynamicCircleScreen(),
      PublishView(),
      MallScreen(),
      MineScreen(),
    ];
  }
}
