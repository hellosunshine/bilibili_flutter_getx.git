import 'package:bilibili_getx/core/permission/bilibili_permission.dart';
import 'package:bilibili_getx/core/system/system_preferred_orientations/system_preferred_orientations_util.dart';
import 'package:bilibili_getx/ui/pages/main/main_view.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

import 'main_state.dart';
import 'publish/publish_view.dart';

class MainLogic extends GetxController {
  final MainState state = MainState();

  @override
  void onInit() {
    SystemPreferredOrientationsUtil.setVertical();
    super.onInit();
  }

  ///更新当前下标
  void updateCurrentIndex(int i) {
    state.currentIndex = i;
    update();
  }

  /// 底部导航菜单
  void onTapBottomMenu(int index) async {
    ///发布界面
    if (index == 2) {
      if (defaultTargetPlatform == TargetPlatform.android) {
        /// 存储权限获取
        final androidInfo = await DeviceInfoPlugin().androidInfo;
        if (androidInfo.version.sdkInt <= 32) {
          final value = await BilibiliPermission().requestStoragePermissions();
          if (value) {
            Get.toNamed("${MainView.routeName}${PublishView.routeName}");
          }
        } else {
          final value = await BilibiliPermission().requestPhotosPermissions();
          if (value) {
            Get.toNamed("${MainView.routeName}${PublishView.routeName}");
          }
        }
      } else {
        Get.toNamed("${MainView.routeName}${PublishView.routeName}");
      }
    } else {
      updateCurrentIndex(index);
    }
  }
}
