import 'package:bilibili_getx/core/I18n/str_res_keys.dart';
import 'package:bilibili_getx/core/constant_util/size.dart';
import 'package:bilibili_getx/core/constant_util/app_theme.dart';
import 'package:bilibili_getx/core/responsive_layout/extension_context_responsive/extension_context.dart';
import 'package:bilibili_getx/ui/shared/image_asset.dart';
import 'package:bilibili_getx/ui/shared/math_compute.dart';
import 'package:bilibili_getx/ui/widgets/custom/bangumi_swiper_pagination.dart';
import 'package:bilibili_getx/ui/widgets/custom/fade_image_default.dart';
import 'package:bilibili_getx/ui/widgets/custom/price_mark.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_swiper_null_safety_flutter3/flutter_swiper_null_safety_flutter3.dart';
import 'package:get/get.dart';

import 'mall_logic.dart';

class MallScreen extends StatefulWidget {
  static const String routeName = "/mall";

  MallScreen({super.key});

  @override
  State<MallScreen> createState() => _MallScreenState();
}

class _MallScreenState extends State<MallScreen>
    with
        AutomaticKeepAliveClientMixin,
        SizeConstantUtil,
        HYAppTheme,
        ImageAssets {
  final logic = Get.find<MallLogic>();
  final state = Get.find<MallLogic>().state;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SafeArea(
      child: GetBuilder<MallLogic>(
        builder: (logic) {
          if (state.isLoadingMallData == false) {
            return initAndroidMallView();
          } else {
            return Container(
              margin: EdgeInsets.only(top: 30.r),
              alignment: Alignment.topCenter,
              width: 1.sw,
              child: RefreshProgressIndicator(
                value: null,
                color: norMainThemeColors,
              ),
            );
          }
        },
      ),
    );
  }

  ///初始化android界面
  Widget initAndroidMallView() {
    return NotificationListener(
      onNotification: (ScrollNotification notification) {
        logic.hideTitle(notification);
        return false;
      },
      child: Scaffold(
        body: RefreshIndicator(
          onRefresh: () async {
            logic.androidFetchMallData();
          },
          child: CustomScrollView(
            physics: const ClampingScrollPhysics(),
            controller: ScrollController(),
            slivers: [
              SliverAppBar(
                backgroundColor: Colors.white,
                expandedHeight: 90.r,
                title: Opacity(
                  opacity: state.appBarOpacity,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        SR.vipMall.tr,
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: norTextColors,
                          fontSize: titleFontSizeCons.r,
                          fontFamily: bF,
                        ),
                      ),
                      5.horizontalSpace,
                      Text(
                        state.vo.slogan,
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: norGrayColor,
                          fontSize: customFontSizeCons.r,
                          fontFamily: bF,
                        ),
                      ),
                    ],
                  ),
                ),
                elevation: .1,
                flexibleSpace: FlexibleSpaceBar(
                  titlePadding: EdgeInsets.symmetric(
                    horizontal: 10.r,
                    vertical: 10.r,
                  ),
                  centerTitle: false,
                  title: Opacity(
                    opacity: 1 - state.appBarOpacity,
                    child: buildAndroidMallViewSimpleSearch(),
                  ),
                  background: Container(
                    alignment: Alignment.bottomCenter,
                    color: norWhite01Color,
                    padding: EdgeInsets.only(left: 10.r, bottom: 10.r),
                    child: buildAndroidMallViewSearch(),
                  ),
                  collapseMode: CollapseMode.parallax,
                ),
                actions: buildAndroidMallViewActions(),
                pinned: true,
                floating: false,
                snap: false,
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (ctx, index) {
                    if (index == 0) {
                      return buildAndroidMallViewSliverListItem01();
                    } else if (index == 1) {
                      return buildAndroidMallViewSliverListItem02();
                    } else if (index == 2) {
                      return buildAndroidMallViewSliverListItem03();
                    } else {
                      return Container();
                    }
                  },
                  childCount: 3,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  ///搜索（顶部的简单搜索框）
  Widget buildAndroidMallViewSimpleSearch() {
    return Container(
      width: 1.sw - 150.r,
      height: 30.r,
      decoration: BoxDecoration(
        color: norWhite07Color,
        borderRadius: BorderRadius.all(Radius.circular(8.r)),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5.r),
            child: Image.asset(
              searchCustomPNG,
              width: customFontSizeCons.r,
              height: customFontSizeCons.r,
            ),
          ),
          Text(
            state.vo.searchUrl.titleList[0],
            style: TextStyle(
              color: norGrayColor,
              fontSize: customFontSizeCons.r,
              fontFamily: bF,
              fontWeight: FontWeight.normal,
            ),
          ),
        ],
      ),
    );
  }

  ///搜索（初始搜索框）
  Widget buildAndroidMallViewSearch() {
    return Row(
      children: [
        Expanded(
          child: Container(
            height: 30.r,
            decoration: BoxDecoration(
              color: norWhite07Color,
              borderRadius: BorderRadius.all(Radius.circular(8.r)),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 5.r),
                      child: Image.asset(
                        searchCustomPNG,
                        width: iconSizeCons.r,
                        height: iconSizeCons.r,
                      ),
                    ),
                    Text(
                      state.vo.searchUrl.titleList[0],
                      style: TextStyle(
                        color: norGrayColor,
                        fontSize: customFontSizeCons.r,
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10.r),
                  child: Image.asset(
                    arPNG,
                    width: iconSizeCons.r,
                    height: iconSizeCons.r,
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          height: iconSizeCons.r,
          width: iconSizeCons.r,
          margin: EdgeInsets.symmetric(horizontal: 10.r),
          child: FadeInImage(
            placeholder: AssetImage(icUpperVideoDefaultPNG),
            image: NetworkImage(
                "http://${state.vo.categoryTabVo.imageUrl.substring(2)}"),
            fit: BoxFit.cover,
            placeholderFit: BoxFit.cover,
          ),
        ),
      ],
    );
  }

  ///actions
  List<Widget> buildAndroidMallViewActions() {
    List<Widget> actions = [];
    for (var action in state.vo.entryList) {
      actions.add(
        Container(
          width: iconSizeCons.r,
          height: iconSizeCons.r,
          margin: EdgeInsets.only(right: 15.r),
          child: FadeInImage(
            placeholder: AssetImage(icUpperVideoDefaultPNG),
            image: NetworkImage("http://${action.imgUrl.substring(2)}"),
            fit: BoxFit.contain,
            placeholderFit: BoxFit.contain,
          ),
        ),
      );
    }
    return actions;
  }

  ///第一大部分
  Widget buildAndroidMallViewSliverListItem01() {
    return NotificationListener(
      onNotification: (ScrollNotification notification) {
        logic.expandSwiperHeight(notification);
        return false;
      },
      child: SizedBox(
        width: 1.sw,
        height: state.swiperHeight,
        child: Swiper(
          loop: false,
          pagination: SwiperPagination(
            alignment: Alignment.bottomCenter,
            builder: SwiperCustomPagination(
              builder: (ctx, config) {
                return RoundRectSwiperPagination(
                  currentIndex: config.activeIndex,
                  itemCount: 2,
                );
              },
            ),
          ),
          itemBuilder: (ctx, index) {
            if (index == 0) {
              List<Widget> children = [];
              for (var item in state.vo.ipTabVo.ipTabs) {
                Widget child = Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    Container(height: 75.r),
                    Container(
                      decoration: BoxDecoration(boxShadow: [
                        BoxShadow(
                          color: norTextColors.withOpacity(.1),
                          offset: const Offset(1, 1),
                          spreadRadius: 1,
                          blurRadius: 1,
                        )
                      ]),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(5.r)),
                        child: DefaultFadeImage(
                          height: 56.r,
                          imageUrl: getImageHttpUrl(item.bgImage),
                          width: (1.sw - 50.r) / 3,
                        ),
                      ),
                    ),
                    Positioned(
                      right: 0,
                      bottom: 0,
                      child: DefaultFadeImage(
                        imageUrl: getImageHttpUrl(item.itemImage),
                        width: 50.r,
                        height: 72.r,
                      ),
                    ),
                    Positioned(
                      left: 3.r,
                      bottom: 3.r,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            item.name,
                            style: TextStyle(
                              color: norTextColors,
                              fontSize: titleFontSizeCons.r,
                            ),
                          ),
                          25.verticalSpace,
                          Text(
                            item.imgTag,
                            style: TextStyle(
                              color: norTextColors,
                              fontSize: customFontSizeCons.r,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                );
                children.add(child);
              }
              return Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: children,
              );
            } else {
              return Wrap(
                spacing: 25.r,
                runSpacing: 5.r,
                alignment: WrapAlignment.spaceEvenly,
                children: state.vo.ipTabVo.subIpTabs.map((e) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(4.r)),
                        child: FadeInImage(
                          width: (1.sw - 120.r) / 5 - 10.r,
                          height: (1.sw - 120.r) / 5 - 10.r,
                          fit: BoxFit.cover,
                          placeholderFit: BoxFit.cover,
                          placeholder: AssetImage(icUpperVideoDefaultPNG),
                          image: NetworkImage(getImageHttpUrl(e.imageUrl)),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10.r),
                        child: Text(
                          e.name,
                          style: TextStyle(
                            color: norTextColors,
                            fontSize: customFontSizeCons.r,
                            fontWeight: FontWeight.normal,
                            fontFamily: bF,
                          ),
                        ),
                      )
                    ],
                  );
                }).toList(),
              );
            }
          },
          itemCount: 2,
        ),
      ),
    );
  }

  ///第二大部分（手办、周边.....）
  Widget buildAndroidMallViewSliverListItem02() {
    List<Widget> children = [];
    for (var item in state.vo.tabs) {
      Widget child = Container(
        padding: EdgeInsets.symmetric(horizontal: 15.r),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            DefaultFadeImage(
              imageUrl: getImageHttpUrl(item.imageUrl),
              width: 40.r,
              height: 40.r,
            ),
            Container(
              margin: EdgeInsets.only(top: 4.r),
              child: Text(
                item.name,
                style: TextStyle(
                  fontSize: customFontSizeCons.r,
                  color: norTextColors,
                  fontWeight: FontWeight.normal,
                  fontFamily: bF,
                ),
              ),
            )
          ],
        ),
      );
      children.add(child);
    }
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.r),
      height: 70.r,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: children,
      ),
    );
  }

  ///瀑布流布局（轮播图、一大块、四小块，两个一行的多块）
  Widget buildAndroidMallViewSliverListItem03() {
    List<Widget> children = [];
    children.add(StaggeredGridTile.count(
      crossAxisCellCount: 2,
      mainAxisCellCount: 1.6,
      child: buildAndroidMallViewSliverListItem0301(),
    ));
    children.add(StaggeredGridTile.count(
      crossAxisCellCount: 2,
      mainAxisCellCount: 1.6,
      child: buildAndroidMallViewSliverListItem0302(),
    ));
    children.add(StaggeredGridTile.count(
      crossAxisCellCount: 1,
      mainAxisCellCount: 1,
      child: buildAndroidMallViewSliverListItem0303(1),
    ));
    children.add(StaggeredGridTile.count(
      crossAxisCellCount: 1,
      mainAxisCellCount: 1,
      child: buildAndroidMallViewSliverListItem0303(2),
    ));
    children.add(StaggeredGridTile.count(
      crossAxisCellCount: 1,
      mainAxisCellCount: 1,
      child: buildAndroidMallViewSliverListItem0303(3),
    ));
    children.add(StaggeredGridTile.count(
      crossAxisCellCount: 1,
      mainAxisCellCount: 1,
      child: buildAndroidMallViewSliverListItem0303(4),
    ));
    children.addAll(buildAndroidMallViewSliverListItem0304());

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.r),
      child: StaggeredGrid.count(
        crossAxisCount: 4,
        mainAxisSpacing: 5.r,
        crossAxisSpacing: 5.r,
        children: children,
      ),
    );
  }

  ///第三部分的第1个子部分（轮播图）
  Widget buildAndroidMallViewSliverListItem0301() {
    return Swiper(
      autoplay: true,
      pagination: SwiperPagination(
        builder: DotSwiperPaginationBuilder(
          activeColor: norMainThemeColors,
          color: norWhite01Color,
          size: 5.r,
          activeSize: 6.r,
          space: 2.r,
        ),
      ),
      itemBuilder: (ctx, index) {
        return ClipRRect(
          borderRadius: BorderRadius.circular(5.r),
          child: DefaultFadeImage(
            imageUrl: getImageHttpUrl(state.vo.banners[index].pic),
          ),
        );
      },
      itemCount: state.vo.banners.length,
    );
  }

  ///第三部分的第2个子部分（一大块）
  Widget buildAndroidMallViewSliverListItem0302() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          width: 1.sw / 2,
          padding: EdgeInsets.all(10.r),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5.r)),
            gradient: LinearGradient(
              begin: const Alignment(0, -1),
              end: const Alignment(0, 1),
              colors: [
                Colors.deepPurple.withOpacity(.2),
                Colors.deepPurple.withOpacity(0)
              ],
            ),
          ),
          child: RichText(
            text: TextSpan(children: [
              TextSpan(
                text: state.vo.newBlocks[0].title,
                style: TextStyle(
                  color: norTextColors,
                  fontWeight: FontWeight.normal,
                  fontFamily: bF,
                  fontSize: customFontSizeCons.r,
                ),
              ),
              TextSpan(
                text: "  ${state.vo.newBlocks[0].tags![0]}",
                style: TextStyle(
                  color: norGrayColor,
                  fontWeight: FontWeight.normal,
                  fontFamily: bF,
                  fontSize: customFontSizeCons.r,
                ),
              ),
            ]),
          ),
        ),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              buildAndroidMallViewSliverListItem0302Cover(0),
              buildAndroidMallViewSliverListItem0302Cover(1),
            ],
          ),
        )
      ],
    );
  }

  Widget buildAndroidMallViewSliverListItem0302Cover(int index) {
    final d = state.vo.newBlocks[0].blockItemVOs[index];
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        DefaultFadeImage(
          imageUrl: getImageHttpUrl(d.imageUrl),
          width: 1.sw / 4 - 20.r,
          height: 1.sw / 4 - 20.r,
        ),
        d.priceSymbol != null
            ? Container(
                margin: EdgeInsets.only(top: 5.r),
                child: PriceMark(
                  text: (d.priceSymbol! == "¥" ? "￥" : d.priceSymbol!) +
                      d.priceDesc![0],
                  color: Colors.deepPurple.withOpacity(.7),
                ),
              )
            : Container(),
      ],
    );
  }

  ///第三部分的第3个子部分（四小块）
  Widget buildAndroidMallViewSliverListItem0303(index) {
    final benefitInfo = state.vo.newBlocks[index].blockItemVOs[0].benefitInfo;
    List<Color> colors = [
      norTextColors,
      norYellow04Colors,
      norYellow05Colors,
      norBlue05Colors,
    ];
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: norTextColors.withOpacity(.1),
            offset: const Offset(1, 1),
            spreadRadius: .5,
            blurRadius: 1,
          )
        ],
        color: norWhite01Color,
        borderRadius: BorderRadius.all(Radius.circular(3.r)),
      ),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                state.vo.newBlocks[index].title,
                style: TextStyle(
                  color: norTextColors,
                  fontSize: customFontSizeCons.r,
                  fontFamily: bF,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 2.r),
                child: DefaultFadeImage(
                  imageUrl: getImageHttpUrl(
                      state.vo.newBlocks[index].blockItemVOs[0].imageUrl),
                  height: 1.sw / 4 - 40.r,
                  width: 1.sw / 4 - 40.r,
                ),
              )
            ],
          ),
          benefitInfo != null && benefitInfo.partOne != null
              ? Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    alignment: Alignment.center,
                    width: 1.sw,
                    padding: EdgeInsets.symmetric(
                      horizontal: 6.r,
                      vertical: 2.r,
                    ),
                    decoration: BoxDecoration(
                      color: norGrayColor.withOpacity(.2),
                      borderRadius: BorderRadius.all(Radius.circular(5.r)),
                    ),
                    child: Text(
                      benefitInfo.partOne! + benefitInfo.partTwo!,
                      style: TextStyle(
                        color: colors[index],
                        fontFamily: bF,
                        fontSize: customFontSizeCons.r,
                        fontWeight: FontWeight.normal,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  ///第四部分（一行两个的最大块部分）
  List<Widget> buildAndroidMallViewSliverListItem0304() {
    List<Widget> feeds = [];
    for (var item in state.vo.feeds.list) {
      List<InlineSpan> titleTagNames = [];
      List<InlineSpan> recommendTagNames = [];
      List<InlineSpan> serviceTagNames = [];
      List<InlineSpan> marketingTagNames = [];
      if (item.tags != null) {
        ///标签（位于标题头部的标签）
        if (item.tags!.titleTagNames != null) {
          for (var tag in item.tags!.titleTagNames!) {
            titleTagNames.add(
              WidgetSpan(
                alignment: PlaceholderAlignment.middle,
                child: Container(
                  margin: EdgeInsets.only(right: 5.r),
                  padding: EdgeInsets.all(2.r),
                  decoration: BoxDecoration(
                      color: norGrayColor.withOpacity(.1),
                      borderRadius: BorderRadius.circular(2.r)),
                  child: Text(
                    tag,
                    style: TextStyle(
                      color: norGrayColor.withOpacity(1),
                      fontSize: customFontSizeCons.r,
                      fontWeight: FontWeight.normal,
                      fontFamily: bF,
                    ),
                  ),
                ),
              ),
            );
          }
        }

        ///标签（标题下一行的标签）recommendTagNames
        if (item.tags!.recommendTagNames != null) {
          for (var tag in item.tags!.recommendTagNames!) {
            recommendTagNames.add(
              WidgetSpan(
                child: Container(
                  margin: EdgeInsets.only(right: 5.r, top: 3.r),
                  padding: EdgeInsets.all(2.r),
                  decoration: BoxDecoration(
                    color: norYellow03Colors.withOpacity(.2),
                    borderRadius: BorderRadius.circular(3.r),
                  ),
                  child: Text(
                    tag,
                    style: TextStyle(
                      color: norYellow03Colors.withOpacity(1),
                      fontSize: customFontSizeCons.r,
                      fontWeight: FontWeight.normal,
                      fontFamily: bF,
                    ),
                  ),
                ),
              ),
            );
          }
        }

        ///标签（标题下一行的标签）serviceTagNames
        if (item.tags!.serviceTagNames != null) {
          for (var tag in item.tags!.serviceTagNames!) {
            serviceTagNames.add(
              WidgetSpan(
                child: Container(
                  margin: EdgeInsets.only(right: 5.r, top: 3.r),
                  padding: EdgeInsets.all(2.r),
                  decoration: BoxDecoration(
                    color: norBlue04Colors.withOpacity(.2),
                    borderRadius: BorderRadius.circular(3.r),
                  ),
                  child: Text(
                    tag,
                    style: TextStyle(
                      color: norBlue04Colors.withOpacity(1),
                      fontSize: customFontSizeCons.r,
                      fontWeight: FontWeight.normal,
                      fontFamily: bF,
                    ),
                  ),
                ),
              ),
            );
          }
        }

        ///标签（标题下一行的标签）marketingTagNames
        if (item.tags!.marketingTagNames != null) {
          for (var tag in item.tags!.marketingTagNames!) {
            marketingTagNames.add(
              WidgetSpan(
                child: Container(
                  margin: EdgeInsets.only(right: 5.r, top: 3.r),
                  padding: EdgeInsets.all(2.r),
                  decoration: BoxDecoration(
                    color: norMainThemeColors.withOpacity(.2),
                    borderRadius: BorderRadius.circular(3.r),
                  ),
                  child: Text(
                    tag,
                    style: TextStyle(
                      color: norMainThemeColors.withOpacity(1),
                      fontSize: customFontSizeCons.r,
                      fontWeight: FontWeight.normal,
                      fontFamily: bF,
                    ),
                  ),
                ),
              ),
            );
          }
        }
      }

      Widget child = StaggeredGridTile.fit(
        crossAxisCellCount:
            context.responsive<int>(2, sm: 2, md: 1, lg: 1, xl: 1),
        child: Card(
          margin: EdgeInsets.zero,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5.r)),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                item.imageUrls != null
                    ? ClipRRect(
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(5.r)),
                        child: DefaultFadeImage(
                          imageUrl: getImageHttpUrl(item.imageUrls![0]),
                          height: 190.r,
                        ),
                      )
                    : Container(),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.r, horizontal: 5.r),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            item.tags != null
                                ? TextSpan(children: titleTagNames)
                                : const TextSpan(),
                            TextSpan(
                              text: item.title,
                              style: TextStyle(
                                color: norTextColors,
                                fontSize: customFontSizeCons.r,
                                fontWeight: FontWeight.normal,
                                fontFamily: bF,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 2.r, bottom: 10.r),
                        child: Text.rich(
                          TextSpan(
                            children: [
                              item.tags != null
                                  ? TextSpan(children: recommendTagNames)
                                  : const TextSpan(),
                              item.tags != null
                                  ? TextSpan(children: serviceTagNames)
                                  : const TextSpan(),
                              item.tags != null
                                  ? TextSpan(children: marketingTagNames)
                                  : const TextSpan(),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 3.r),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            item.priceDesc != null
                                ? Text.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                          text: "￥",
                                          style: TextStyle(
                                            color: norMainThemeColors,
                                            fontSize: titleFontSizeCons.r,
                                            fontFamily: bF,
                                            fontWeight: FontWeight.normal,
                                          ),
                                        ),
                                        TextSpan(
                                          text: item.priceDesc![0],
                                          style: TextStyle(
                                            color: norMainThemeColors,
                                            fontSize: titleFontSizeCons.r,
                                            fontFamily: bF,
                                            fontWeight: FontWeight.normal,
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                : Container(),
                            item.like != null
                                ? Text.rich(
                                    TextSpan(
                                      children: [
                                        WidgetSpan(
                                          child: Image.asset(heartPNG,
                                              width: iconSizeCons.r,
                                              height: iconSizeCons.r),
                                          alignment:
                                              PlaceholderAlignment.middle,
                                        ),
                                        TextSpan(
                                          text: " ${item.like}",
                                          style: TextStyle(
                                            color: norGrayColor,
                                            fontSize: customFontSizeCons.r,
                                            fontFamily: bF,
                                            fontWeight: FontWeight.normal,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                : Container()
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
      feeds.add(child);
    }
    return feeds;
  }

  ///初始化Web界面
  Widget initWebMallView() {
    return state.result.isNotEmpty
        ? Scaffold(
            body: GridView.builder(
              controller: ScrollController(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisExtent: 120.r,
                mainAxisSpacing: 10.r,
                crossAxisSpacing: 10.r,
              ),
              itemBuilder: (ctx, index) {
                return Stack(
                  alignment: Alignment.bottomLeft,
                  children: [
                    buildMallItemBackGround(index),
                    buildMallItemContent(index),
                  ],
                );
              },
              itemCount: state.total,
            ),
          )
        : Container(
            margin: EdgeInsets.only(top: 30.r),
            alignment: Alignment.topCenter,
            width: 1.sw,
            child: RefreshProgressIndicator(
              value: null,
              color: norMainThemeColors,
            ),
          );
  }

  ///会员购列表的每一项背景
  Widget buildMallItemBackGround(index) {
    return MouseRegion(
      onHover: (event) {
        logic.mouseHoverAction(index);
      },
      onExit: (event) {
        logic.mouseExitAction(index);
      },
      child: Container(
        width: 172.r,
        height: 72.r,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: Colors.black.withOpacity(.1),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(.2),
                offset: state.backgroundOffset[index],
                blurRadius: state.backgroundBlurRadius[index],
                spreadRadius: state.backgroundSpreadRadius[index],
              ),
            ],
            borderRadius: BorderRadius.circular(5.r)),
      ),
    );
  }

  ///内容
  Widget buildMallItemContent(int index) {
    return Positioned(
      left: 6.r,
      bottom: 5.r,
      child: MouseRegion(
        onHover: (event) {
          logic.mouseHoverAction(index);
        },
        onExit: (event) {
          logic.mouseExitAction(index);
        },
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(5.r)),
              child: Container(
                width: 55.r,
                height: 75.r,
                margin: EdgeInsets.only(bottom: state.coverBottomGap[index]),
                decoration: BoxDecoration(
                  color: norWhite01Color,
                  boxShadow: [
                    BoxShadow(
                      color: norTextColors.withOpacity(.3),
                      offset: const Offset(1.0, 1.0),
                      blurRadius: 2,
                      spreadRadius: 2,
                    )
                  ],
                ),
                child: FadeInImage(
                  placeholder: AssetImage(icUpperVideoDefaultPNG),
                  image: NetworkImage("http:${state.result[index].cover}"),
                  fit: BoxFit.cover,
                  placeholderFit: BoxFit.cover,
                ),
              ),
            ),
            8.horizontalSpace,
            Container(
              margin: EdgeInsets.only(bottom: 5.r),
              height: 52.r,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 100.r,
                    child: Text(
                      state.result[index].projectName,
                      style: TextStyle(
                        fontSize: 14.sp,
                        color: norTextColors,
                      ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        height: 10.sp,
                        width: 10.sp,
                        child: Image.asset(schedulePNG),
                      ),
                      3.horizontalSpace,
                      SizedBox(
                        height: 10.sp,
                        child: Text(
                          "${state.result[index].startTime} - ${state.result[index].endTime}",
                          style: TextStyle(
                            fontSize: 10.sp,
                            color: norGrayColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        height: 10.sp,
                        width: 10.sp,
                        child: Image.asset(locationPNG),
                      ),
                      3.horizontalSpace,
                      SizedBox(
                        height: 10.sp,
                        child: Text(
                          state.result[index].venueName,
                          style: TextStyle(
                            fontSize: 10.sp,
                            color: norGrayColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                                text: "￥",
                                style: TextStyle(
                                  color: norMainThemeColors,
                                  fontSize: 14.sp,
                                )),
                            TextSpan(
                              text: (state.result[index].priceLow ~/ 100)
                                  .toString(),
                              style: TextStyle(
                                color: norMainThemeColors,
                                fontSize: 20.sp,
                              ),
                            ),
                            TextSpan(
                              text: "  ${SR.up.tr}  ",
                              style: TextStyle(
                                color: norGrayColor,
                                fontSize: 10.sp,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(2.r),
                        decoration: BoxDecoration(
                            border: Border.all(color: norMainThemeColors)),
                        child: Text(
                          SR.exclusive.tr,
                          style: TextStyle(
                            color: norMainThemeColors,
                            fontSize: 10.sp,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
