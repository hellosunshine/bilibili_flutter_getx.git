import 'package:bilibili_getx/core/constant_util/size.dart';
import 'package:bilibili_getx/ui/shared/image_asset.dart';
import 'package:bilibili_getx/ui/widgets/custom/auto_keep_alive_wrapper.dart';
import 'package:bilibili_getx/ui/widgets/custom/circle_inkwell_button.dart';
import 'package:bilibili_getx/ui/widgets/custom/expanded_widget.dart';
import 'package:bilibili_getx/ui/widgets/custom/fade_image_default.dart';
import 'package:bilibili_getx/ui/widgets/custom/primary_scroll_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../core/I18n/str_res_keys.dart';
import '../../../core/model/android/video_play/video_profile_model.dart';
import '../../../core/constant_util/app_theme.dart';
import '../../shared/math_compute.dart';
import '../../widgets/custom/video_reply_item.dart';
import 'bilibili_video_player/bilibili_video_player_view.dart';
import 'video_play_logic.dart';

final logic = Get.find<VideoPlayLogic>();
final state = Get.find<VideoPlayLogic>().state;

class VideoPlayScreen extends StatefulWidget {
  static const String routeName = "/video_play";

  const VideoPlayScreen({super.key});

  @override
  State<VideoPlayScreen> createState() => _VideoPlayScreenState();
}

class _VideoPlayScreenState extends State<VideoPlayScreen>
    with
        SingleTickerProviderStateMixin,
        HYAppTheme,
        ImageAssets,
        SizeConstantUtil {
  late TabController tabController;
  late bool haveFullScreenFunction;
  late bool haveFinishView;
  late String videoOriginalUrl;
  late String cid;
  late String oid;
  late bool initVideoPlayerVideoData;
  late bool initVideoPlayerDanMuData;
  late bool initVideoControllerAndDanMuController;
  late int fetchDanMu;

  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    tabController.addListener(() {
      for (int i = 0; i < state.scrollChildKeys.length; i++) {
        GlobalKey<PrimaryScrollContainerState> key = state.scrollChildKeys[i];
        if (key.currentState != null) {
          key.currentState?.onPageChange(tabController.index == i); //控制是否当前显示
        }
      }
    });

    ///控制显示或者隐藏
    state.nestedScrollViewController.addListener(() {
      if (state.nestedScrollViewController.offset > 140.w &&
          !state.nestedScrollViewController.position.outOfRange &&
          state.showOrHideIconAndTitleOpacity != 1) {
        state.showOrHideIconAndTitleOpacity = 1;
        logic.update();
      }
      if (state.nestedScrollViewController.offset < 140.w &&
          !state.nestedScrollViewController.position.outOfRange &&
          state.showOrHideIconAndTitleOpacity != 0) {
        state.showOrHideIconAndTitleOpacity = 0;
        logic.update();
      }
    });

    /// 接收参数
    if (Get.arguments != null && Get.arguments["aid"] != null) {
      state.aid = Get.arguments["aid"];
    }
    if (Get.arguments != null && Get.arguments["initVideoPlayData"] != null) {
      logic.initVideoPlayData();
    }
    if (Get.arguments != null && Get.arguments["changeVideoState"] != null) {
      logic.changeVideoState(Get.arguments["changeVideoState"]);
    }
    if (Get.arguments != null && Get.arguments["fetchVideoView"] != null) {
      logic.fetchVideoView();
    }
    if (Get.arguments != null && Get.arguments["fetchVideoReply"] != null) {
      logic.fetchVideoReply();
    }

    if (Get.arguments != null) {
      if (Get.arguments["haveFullScreenFunction"] != null) {
        haveFullScreenFunction = Get.arguments["haveFullScreenFunction"];
      }
      if (Get.arguments["haveFinishView"] != null) {
        haveFinishView = Get.arguments["haveFinishView"];
      }
      if (Get.arguments["videoOriginalUrl"] != null) {
        videoOriginalUrl = Get.arguments["videoOriginalUrl"];
      }
      if (Get.arguments["cid"] != null) {
        cid = Get.arguments["cid"];
      }
      if (Get.arguments["oid"] != null) {
        oid = Get.arguments["oid"];
      }
      if (Get.arguments["initVideoPlayerVideoData"] != null) {
        initVideoPlayerVideoData = Get.arguments["initVideoPlayerVideoData"];
      }
      if (Get.arguments["initVideoPlayerDanMuData"] != null) {
        initVideoPlayerDanMuData = Get.arguments["initVideoPlayerDanMuData"];
      }
      if (Get.arguments["initVideoControllerAndDanMuController"] != null) {
        initVideoControllerAndDanMuController =
            Get.arguments["initVideoControllerAndDanMuController"];
      }
      if (Get.arguments["fetchDanMu"] != null) {
        fetchDanMu = Get.arguments["fetchDanMu"];
      }
    }
    super.initState();
  }

  @override
  void dispose() {
    tabController.removeListener(() {});
    tabController.dispose();
    state.nestedScrollViewController.removeListener(() {});
    state.nestedScrollViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GetBuilder<VideoPlayLogic>(builder: (logic) {
        return PopScope(
          onPopInvoked: (v) {
            Get.back();
          },
          child: Scaffold(
            body: !state.videoIsFinished
                ? buildVideoPlayingView()
                : buildVideoStopPlayingView(),
          ),
        );
      }),
    );
  }

  ///播放时的界面
  Widget buildVideoPlayingView() {
    return NestedScrollView(
      physics: const NeverScrollableScrollPhysics(),
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return [
          SliverAppBar(
            leading: null,
            automaticallyImplyLeading: false,
            elevation: 0.1,
            pinned: true,
            floating: false,
            snap: false,
            collapsedHeight: 180.w,
            flexibleSpace: FlexibleSpaceBar(
              background: buildVideoPlayVideoPlayer(),
            ),
          ),
          SliverAppBar(
            elevation: 0.1,
            leading: null,
            automaticallyImplyLeading: false,
            toolbarHeight: 40.w,
            backgroundColor: norWhite01Color,
            title: buildVideoPlayTabBar(),
            pinned: true,
            floating: false,
            snap: false,
          ),
        ];
      },
      body: buildVideoPlayTabBarView(),
    );
  }

  ///播放后的界面
  Widget buildVideoStopPlayingView() {
    return NestedScrollView(
      controller: state.nestedScrollViewController,
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return [
          SliverAppBar(
            backgroundColor: norMainThemeColors,
            pinned: true,
            floating: false,
            snap: false,
            centerTitle:
                state.showOrHideIconAndTitleOpacity == 1 ? true : false,
            leadingWidth: state.showOrHideIconAndTitleOpacity == 1 ? 40.r : 0,
            leading: state.showOrHideIconAndTitleOpacity == 1
                ? Icon(
                    Icons.arrow_back_ios_rounded,
                    color: norWhite01Color,
                    size: iconSizeCons.r,
                  )
                : null,
            actions: [
              Container(
                margin: EdgeInsets.only(right: 20.r),
                child: Image.asset(
                  width: 20.r,
                  height: 20.r,
                  moreAndroidLightPNG,
                ),
              )
            ],
            title: Opacity(
              opacity: 1 - state.showOrHideIconAndTitleOpacity,
              child: Image.asset(
                videoHomePNG,
                width: 20.r,
                height: 20.r,
                fit: BoxFit.cover,
              ),
            ),
            expandedHeight: 250.r,
            flexibleSpace: FlexibleSpaceBar(
              title: Opacity(
                opacity: state.showOrHideIconAndTitleOpacity,
                child: Padding(
                  padding: EdgeInsets.only(right: 80.r),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 10.r),
                        child: Image.asset(
                          playVideoCustomPNG,
                          width: iconSizeCons.r,
                          height: iconSizeCons.r,
                        ),
                      ),
                      Text(
                        "立即播放",
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: titleFontSizeCons.r,
                          color: norWhite01Color,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              collapseMode: CollapseMode.none,
              background: state.videoProfile != null
                  ? buildVideoPlayVideoRecommend()
                  : null,
            ),
          ),
          SliverAppBar(
            leading: null,
            automaticallyImplyLeading: false,
            toolbarHeight: 40.r,
            // backgroundColor: Colors.red,
            backgroundColor: norWhite01Color,
            title: buildVideoPlayTabBar(),
            pinned: true,
            floating: false,
            snap: false,
          ),
        ];
      },
      body: buildVideoPlayTabBarView(),
    );
  }

  ///视频和弹幕
  Widget buildVideoPlayVideoPlayer() {
    return BilibiliVideoPlayer(
      haveFullScreenFunction: haveFullScreenFunction,
      haveFinishView: haveFinishView,
      videoOriginalUrl: videoOriginalUrl,
      cid: cid,
      oid: oid,
      initVideoPlayerVideoData: initVideoPlayerVideoData,
      initVideoPlayerDanMuData: initVideoPlayerDanMuData,
      initVideoControllerAndDanMuController:
          initVideoControllerAndDanMuController,
      fetchDanMu: fetchDanMu,
      danMuOpenOrClose: state.danMuOpenOrClose,
    );
  }

  ///播放结束后推荐其他视频
  Widget buildVideoPlayVideoRecommend() {
    ///第一个推荐可能为非视频内容
    final relates = state.videoProfile!.relates!;
    Relate relate = relates[0];
    if (relate.goto == "av") {
      relate = relates[0];
    } else {
      relate = relates[1];
    }
    return Container(
      padding: EdgeInsets.only(top: 40.r, left: 15.r, right: 8.r),
      color: norBlackColors,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "推荐视频",
            style: TextStyle(
              color: norGrayColor,
              fontSize: titleFontSizeCons.r,
            ),
          ),
          Row(
            children: [
              Stack(
                alignment: Alignment.center,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5.r),
                    child: DefaultFadeImage(
                      imageUrl: relate.pic!,
                      width: 130.r,
                      height: 75.r,
                    ),
                  ),
                  Image.asset(
                    playVideoCustomPNG,
                    width: 30.r,
                    height: 30.r,
                  ),
                  Positioned(
                    right: 5.r,
                    bottom: 3.r,
                    child: Container(
                      decoration: BoxDecoration(
                          color: norTextColors.withOpacity(.4),
                          borderRadius: BorderRadius.circular(3.r)),
                      padding:
                          EdgeInsets.symmetric(horizontal: 5.r, vertical: 2.r),
                      child: Text(
                        changeToDurationText(relate.duration!.toDouble()),
                        style: TextStyle(
                          fontSize: 13.r,
                          color: norWhite01Color,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              10.horizontalSpace,
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      relate.title!,
                      style: TextStyle(
                        color: norWhite01Color,
                        fontSize: titleFontSizeCons.r,
                      ),
                    ),
                    10.verticalSpace,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  width: customFontSizeCons.r,
                                  height: customFontSizeCons.r,
                                  child: Image.asset(upGrayPNG),
                                ),
                                5.horizontalSpace,
                                Text(
                                  relate.owner.name!,
                                  style: TextStyle(
                                    color: norGrayColor,
                                    fontSize: customFontSizeCons.r,
                                  ),
                                )
                              ],
                            ),
                            5.verticalSpace,
                            Text.rich(
                              TextSpan(
                                children: [
                                  WidgetSpan(
                                    alignment: PlaceholderAlignment.middle,
                                    child: Image.asset(
                                      iconListPlayerPNG,
                                      width: customFontSizeCons.r,
                                      height: customFontSizeCons.r,
                                    ),
                                  ),
                                  TextSpan(
                                    text:
                                        " ${changeToWan(relate.stat["view"]!)}  ",
                                    style: TextStyle(
                                      fontSize: customFontSizeCons.r,
                                      color: norGrayColor,
                                    ),
                                  ),
                                  WidgetSpan(
                                    alignment: PlaceholderAlignment.middle,
                                    child: Image.asset(
                                      icDanmuWhitePNG,
                                      width: customFontSizeCons.r,
                                      height: customFontSizeCons.r,
                                    ),
                                  ),
                                  TextSpan(
                                    text:
                                        " ${changeToWan(relate.stat["danmaku"]!)}",
                                    style: TextStyle(
                                      fontSize: customFontSizeCons.r,
                                      color: norGrayColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: norGrayColor.withOpacity(.8),
                            borderRadius:
                                BorderRadius.all(Radius.circular(3.r)),
                          ),
                          margin: EdgeInsets.only(right: 6.r),
                          padding: EdgeInsets.symmetric(
                            horizontal: 6.r,
                            vertical: 4.r,
                          ),
                          child: Text(
                            "立即播放",
                            style: TextStyle(
                              fontSize: customFontSizeCons.r,
                              color: norWhite01Color,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
          Text.rich(
            TextSpan(
              children: [
                WidgetSpan(
                  child: Image.asset(
                    replayPNG,
                    width: iconSizeCons.r,
                    height: iconSizeCons.r,
                  ),
                ),
                TextSpan(
                  text: " 重播  ",
                  style: TextStyle(
                    color: norWhite01Color,
                    fontSize: titleFontSizeCons.r,
                  ),
                ),
                WidgetSpan(
                  child: Image.asset(
                    sharePNG,
                    width: iconSizeCons.r,
                    height: iconSizeCons.r,
                  ),
                ),
                TextSpan(
                  text: " 分享",
                  style: TextStyle(
                    color: norWhite01Color,
                    fontSize: titleFontSizeCons.r,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  ///TabBar
  PreferredSizeWidget buildVideoPlayTabBar() {
    return PreferredSize(
      ///tab设置底色
      preferredSize: Size(1.sw, 1.sh),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: 100.r,
            height: 40.r,
            child: TabBar(
              controller: tabController,
              tabs: [
                Tab(text: SR.profile.tr),
                Tab(text: SR.reply.tr),
              ],
              indicatorColor: norMainThemeColors,
              unselectedLabelColor: unselectedLabelColor,
              labelColor: norMainThemeColors,
              indicatorSize: TabBarIndicatorSize.tab,
              labelStyle: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: titleFontSizeCons.r,
                fontFamily: bF,
              ),
              unselectedLabelStyle: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: titleFontSizeCons.r,
                fontFamily: bF,
              ),
              labelPadding: EdgeInsets.zero,
              indicatorPadding: const EdgeInsets.only(
                bottom: 6,
                left: 10,
                right: 10,
              ).r,
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                alignment: Alignment.center,
                width: 70.r,
                height: 30.r,
                decoration: BoxDecoration(
                  color: norGrayColor.withOpacity(.3),
                  borderRadius:
                      BorderRadius.horizontal(left: Radius.circular(20.r)),
                ),
                child: Text(
                  "点我发弹幕",
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: norGrayColor,
                    fontSize: customFontSizeCons.r,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  ///点击显示弹幕或隐藏弹幕
                  logic.showOrHideDanMu();
                },
                child: Container(
                  alignment: Alignment.center,
                  width: 40.r,
                  height: 30.r,
                  decoration: BoxDecoration(
                    color: norGrayColor.withOpacity(.2),
                    borderRadius: BorderRadius.horizontal(
                      right: Radius.circular(20.r),
                    ),
                  ),
                  child: Image.asset(
                    danmuOpenPNG,
                    width: iconSizeCons.r,
                    height: iconSizeCons.r,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  ///TabBarView
  Widget buildVideoPlayTabBarView() {
    return TabBarView(
      controller: tabController,
      children: [
        state.isLoadingVideoProfile
            ? Center(
                child: CircularProgressIndicator(color: norMainThemeColors))
            : buildVideoProfile(),
        state.isLoadingVideoReply
            ? Center(
                child: CircularProgressIndicator(color: norMainThemeColors))
            : buildVideoPlayComments(),
      ],
    );
  }

  Widget buildVideoPlayComments() {
    return state.allReplies.isEmpty
        ? const Center(
            child: Text("没有评论"),
          )
        : PrimaryScrollContainer(
            key: state.scrollChildKeys[1],
            child: ListView.separated(
              padding: EdgeInsets.symmetric(horizontal: 15.r, vertical: 10.r),
              shrinkWrap: true,
              itemBuilder: (ctx, index) {
                if (index == 0) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      buildVideoReplyTitle(),
                      10.verticalSpace,
                      VideoReplyItem(state.allReplies[index])
                    ],
                  );
                }
                return VideoReplyItem(state.allReplies[index]);
              },
              separatorBuilder: (ctx, index) {
                if (index == state.allReplies.length) {
                  return Center(
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10)
                          .r,
                      child: Text(
                        "再怎么找也没有啦~",
                        style: TextStyle(
                          color: norGrayColor.withOpacity(.5),
                          fontSize: 14.sp,
                        ),
                      ),
                    ),
                  );
                }
                return Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 20)
                          .r,
                  child: Divider(
                    color: norGrayColor.withOpacity(.5),
                  ),
                );
              },
              itemCount: state.allReplies.length,
            ),
          );
  }

  ///热门评论
  Widget buildVideoReplyTitle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          state.videoReply.cursor.name,
          style: TextStyle(color: norTextColors, fontSize: 14.sp),
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              icFollowDecPNG,
              width: 20.w,
              height: 20.h,
            ),
            Text(
              "按热度",
              style: TextStyle(color: norGrayColor, fontSize: 14.sp),
            ),
          ],
        ),
      ],
    );
  }

  buildVideoProfile() {
    return AutoKeepAliveWrapper(
      child: PrimaryScrollContainer(
        key: state.scrollChildKeys[0],
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15.r, vertical: 10.r),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                buildVideoPlayVideoInfoOwnerInfo(
                  state.videoProfile!.owner!.face!,
                  state.videoProfile!.owner!.name!,
                ),
                20.verticalSpace,
                buildVideoPlayVideoInfoVideoTitle(),
                8.verticalSpace,
                ExpandedWidget(
                  expandedKey: state.cutDownWidgetKey,
                  defaultHeight: 0,
                  child: buildVideoPlayVideoInfoVideoDetails(),
                ),
                30.verticalSpace,
                buildVideoPlayVideoInfoButtonBanner(),
                60.verticalSpace,

                ///推荐视频
                buildVideoRecommendList(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildVideoPlayVideoInfoOwnerInfo(String ownerIcon, String ownerName) {
    return Row(
      children: [
        buildVideoPlayUserIcon(ownerIcon),
        Expanded(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10).r,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  ownerName,
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: norMainThemeColors,
                    fontSize: titleFontSizeCons.r,
                  ),
                ),
                3.verticalSpace,
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${changeToWan(state.videoProfile!.ownerExt!.fans!)}粉丝",
                      style: TextStyle(
                        color: norGrayColor,
                        fontSize: subTitleFontSizeCons.r,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    10.horizontalSpace,
                    Text(
                      "${state.videoProfile!.videos} 视频",
                      style: TextStyle(
                        color: norGrayColor,
                        fontSize: subTitleFontSizeCons.r,
                        fontWeight: FontWeight.normal,
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
        TextButton(
          onPressed: () {},
          style: TextButton.styleFrom(
            padding: const EdgeInsets.symmetric(horizontal: 13).r,
            backgroundColor: norMainThemeColors,
            textStyle: TextStyle(
              fontSize: customFontSizeCons.r,
              fontWeight: FontWeight.normal,
              color: norWhite01Color,
            ),
          ),
          child: Text(
            "+  关注",
            style: TextStyle(
              fontSize: titleFontSizeCons.r,
              fontWeight: FontWeight.normal,
              color: norWhite01Color,
              fontFamily: bF,
            ),
          ),
        )
      ],
    );
  }

  ///视频标题
  Widget buildVideoPlayVideoInfoVideoTitle() {
    String endDateText = getPubData(state.videoProfile!.pubdate!).toString();
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        GestureDetector(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: !state.isExpanded
                    ? Text(
                        state.videoProfile!.title!,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: norTextColors,
                          fontSize: titleFontSizeCons.r,
                          fontWeight: FontWeight.normal,
                        ),
                      )
                    : Text(
                        state.videoProfile!.title!,
                        style: TextStyle(
                          color: norTextColors,
                          fontSize: titleFontSizeCons.r,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
              ),
              10.horizontalSpace,
              state.isExpanded
                  ? Image.asset(
                      icArrowUpPNG,
                      width: titleFontSizeCons.r,
                      height: titleFontSizeCons.r,
                    )
                  : Image.asset(
                      icArrowDownPNG,
                      width: titleFontSizeCons.r,
                      height: titleFontSizeCons.r,
                    ),
            ],
          ),
          onTap: () {
            logic.expandedVideoProfileDetail();
          },
        ),
        10.verticalSpace,
        Row(
          children: [
            buildIconInfo(
              playCustom02Png,
              changeToWan(state.videoProfile!.stat!["view"] ?? 0),
            ),
            10.horizontalSpace,
            buildIconInfo(
              remarkCustom02Png,
              changeToWan(state.videoProfile!.stat!["danmaku"] ?? 0),
            ),
            10.horizontalSpace,
            Text(
              endDateText.substring(0, endDateText.length - 4),
              style: TextStyle(
                color: norGrayColor,
                fontSize: customFontSizeCons.r,
                fontWeight: FontWeight.normal,
              ),
            )
          ],
        ),
      ],
    );
  }

  ///视频详细信息
  Widget buildVideoPlayVideoInfoVideoDetails() {
    late List<Widget> tagWidgets = [];
    for (var tag in state.videoProfile!.tag!) {
      tagWidgets.add(buildVideoTag(tag.tagName));
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              state.videoProfile!.bvid!,
              style: TextStyle(
                color: norGrayColor,
                fontSize: customFontSizeCons.r,
              ),
            ),
            8.horizontalSpace,
            buildIconInfo(banCustomPNG, "未经作者授权禁止转载"),
          ],
        ),
        8.verticalSpace,
        Text(
          state.videoProfile!.desc!,
          style: TextStyle(
            color: norGrayColor,
            fontSize: customFontSizeCons.r,
          ),
        ),
        30.verticalSpace,
        Wrap(
          spacing: 10.r,
          runSpacing: 15.r,
          alignment: WrapAlignment.start,
          children: tagWidgets,
        )
      ],
    );
  }

  ///点赞投币收藏按钮
  Widget buildVideoPlayVideoInfoButtonBanner() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        buildVideoPlayIconButton(
            likeCustomPNG, changeToWan(state.videoProfile!.stat!["like"] ?? 0)),
        buildVideoPlayIconButton(dislikeCustomPNG, "不喜欢"),
        buildVideoPlayIconButton(
            coinCustomPNG, changeToWan(state.videoProfile!.stat!["coin"] ?? 0)),
        buildVideoPlayIconButton(collectCustomPNG,
            changeToWan(state.videoProfile!.stat!["favorite"] ?? 0)),
        buildVideoPlayIconButton(shareCustomPNG,
            changeToWan(state.videoProfile!.stat!["share"] ?? 0)),
      ],
    );
  }

  Widget buildVideoPlayIconButton(String icon, String text) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image.asset(
          icon,
          width: 24.r,
          height: 24.r,
          color: norGray03Color,
        ),
        10.verticalSpace,
        Text(
          text,
          style: TextStyle(
            color: norGrayColor,
            fontSize: customFontSizeCons.r,
            fontWeight: FontWeight.normal,
          ),
        )
      ],
    );
  }

  Widget buildVideoTag(String text) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 6, horizontal: 12).r,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50.r), color: norWhite08Color),
      child: Text(
        text,
        style: TextStyle(
          color: norGray02Color,
          fontWeight: FontWeight.normal,
          fontSize: customFontSizeCons.r,
        ),
      ),
    );
  }

  Widget buildIconInfo(String icon, String text) {
    return Text.rich(TextSpan(
      children: [
        WidgetSpan(
          alignment: PlaceholderAlignment.middle,
          child: Image.asset(
            icon,
            height: subTitleFontSizeCons.r,
            width: subTitleFontSizeCons.r,
          ),
        ),
        TextSpan(
          text: " $text",
          style: TextStyle(
            color: norGrayColor,
            fontSize: titleFontSizeCons.r,
            fontWeight: FontWeight.normal,
          ),
        ),
      ],
    ));
  }

  Widget buildVideoPlayUserIcon(String userLogo) {
    return CircleInkWellButton(
      bgColor: norWhite01Color,
      onTap: () {},
      width: 45.r,
      height: 45.r,
      child: Image.network(
        userLogo,
        fit: BoxFit.cover,
      ),
    );
  }

  ///推荐视频
  Widget buildVideoRecommendList() {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (ctx, index) {
        if (state.videoProfile!.relates![index].goto == "av") {
          return SizedBox(
            width: 1.sw,
            height: 100.r,
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.r)),
                  child: DefaultFadeImage(
                    imageUrl: state.videoProfile!.relates![index].pic!,
                    width: 150.r,
                    height: 80.r,
                    fit: BoxFit.cover,
                  ),
                ),
                10.horizontalSpace,
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        state.videoProfile!.relates![index].title!,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: norTextColors,
                          fontSize: titleFontSizeCons.r,
                        ),
                      ),
                      Text.rich(
                        TextSpan(children: [
                          WidgetSpan(
                            child: Image.asset(
                              uperCustomPNG,
                              width: titleFontSizeCons.r,
                              height: titleFontSizeCons.r,
                            ),
                          ),
                          TextSpan(
                            text:
                                " ${state.videoProfile!.relates![index].owner.name!}",
                            style: TextStyle(
                              overflow: TextOverflow.ellipsis,
                              fontWeight: FontWeight.normal,
                              color: norGrayColor,
                              fontSize: customFontSizeCons.r,
                            ),
                          ),
                        ]),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text.rich(
                            TextSpan(
                              children: [
                                WidgetSpan(
                                  alignment: PlaceholderAlignment.middle,
                                  child: Image.asset(
                                    iconListPlayerPNG,
                                    width: subTitleFontSizeCons.r,
                                    height: subTitleFontSizeCons.r,
                                  ),
                                ),
                                TextSpan(
                                  text:
                                      " ${changeToWan(state.videoProfile!.relates![index].stat["view"]!)}  ",
                                  style: TextStyle(
                                    fontSize: subTitleFontSizeCons.r,
                                    color: norGrayColor,
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),
                                WidgetSpan(
                                  alignment: PlaceholderAlignment.middle,
                                  child: Image.asset(
                                    icDanmuGrayPNG,
                                    width: subTitleFontSizeCons.r,
                                    height: subTitleFontSizeCons.r,
                                  ),
                                ),
                                TextSpan(
                                  text:
                                      " ${state.videoProfile!.relates![index].stat["danmaku"]!}",
                                  style: TextStyle(
                                    fontSize: subTitleFontSizeCons.r,
                                    color: norGrayColor,
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Image.asset(
                            videoMoreCustomPNG,
                            width: subTitleFontSizeCons.r,
                            height: subTitleFontSizeCons.r,
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        } else {
          return SizedBox(
            width: 1.r,
            height: 80.r,
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.r)),
                  child: DefaultFadeImage(
                    imageUrl: state.videoProfile!.relates![index].cover!,
                    width: 150.r,
                    height: 80.r,
                    fit: BoxFit.cover,
                  ),
                ),
                10.horizontalSpace,
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        state.videoProfile!.relates![index].title!,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: norTextColors,
                          fontSize: titleFontSizeCons.r,
                        ),
                      ),
                      Text.rich(TextSpan(
                        children: [
                          WidgetSpan(
                            alignment: PlaceholderAlignment.middle,
                            child: Image.asset(
                              uperCustomPNG,
                              width: subTitleFontSizeCons.r,
                              height: subTitleFontSizeCons.r,
                            ),
                          ),
                          TextSpan(
                            text:
                                " ${state.videoProfile!.relates![index].desc!}",
                            style: TextStyle(
                              overflow: TextOverflow.ellipsis,
                              fontWeight: FontWeight.normal,
                              color: norGrayColor,
                              fontSize: customFontSizeCons.r,
                            ),
                          ),
                        ],
                      )),
                    ],
                  ),
                )
              ],
            ),
          );
        }
      },
      separatorBuilder: (ctx, index) {
        return Container(
          padding: EdgeInsets.only(left: 8.r),
          child: Divider(
            color: norGrayColor.withOpacity(.2),
            thickness: .5.w,
          ),
        );
      },
      itemCount: state.videoProfile!.relates!.length,
    );
  }
}

///视频信息
