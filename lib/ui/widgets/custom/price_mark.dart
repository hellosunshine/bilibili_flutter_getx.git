import 'package:bilibili_getx/core/constant_util/app_theme.dart';
import 'package:bilibili_getx/core/constant_util/size.dart';
import 'package:bilibili_getx/ui/shared/image_asset.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PriceMark extends StatelessWidget
    with HYAppTheme, ImageAssets, SizeConstantUtil {
  final Color color;
  final String text;

  PriceMark({Key? key, required this.color, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ClipPath(
        //定义裁切路径
        clipper: BackgroundClipper(),
        child: buildContainer(context),
      ),
    );
  }

  //一个普通的背景
  Container buildContainer(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: 40.r,
      height: 20.r,
      //背景装饰
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(2.r)),
        //线性渐变
        gradient: LinearGradient(
          //渐变使用到的颜色
          colors: [color, Colors.white],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
        ),
      ),
      child: Text(
        text,
        style: TextStyle(
          fontWeight: FontWeight.normal,
          color: norTextColors,
          fontSize: customFontSizeCons.r,
          fontFamily: bF,
        ),
      ),
    );
  }
}

class BackgroundClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.moveTo(0, size.height * 0.5);
    path.lineTo(size.width * 0.2, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width * 0.2, size.height);
    path.lineTo(0, size.height * 0.5);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
