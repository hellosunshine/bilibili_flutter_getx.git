import 'package:bilibili_getx/core/constant_util/app_theme.dart';
import 'package:bilibili_getx/ui/shared/image_asset.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

///轮播图的底部的指示标志
class BangumiSwiperPagination extends StatefulWidget {
  ///当前的页面的下标
  final int currentIndex;

  ///总共几个页面
  final int itemCount;

  const BangumiSwiperPagination(
      {Key? key, required this.currentIndex, required this.itemCount})
      : super(key: key);

  @override
  State<BangumiSwiperPagination> createState() =>
      _BangumiSwiperPaginationState();
}

class _BangumiSwiperPaginationState extends State<BangumiSwiperPagination>
    with HYAppTheme, ImageAssets {
  late ScrollController _swiperPaginationController;

  @override
  void initState() {
    _swiperPaginationController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    _swiperPaginationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 70.w,
      child: GridView.builder(
        controller: _swiperPaginationController,
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: widget.itemCount,
          childAspectRatio: 5 / 2,
        ),
        itemBuilder: (BuildContext context, int index) {
          return widget.currentIndex == index
              ? Container(
                  decoration: BoxDecoration(
                      color: norMainThemeColors,
                      borderRadius: BorderRadius.circular(5.r)),
                )
              : CircleAvatar(
                  backgroundColor: norGrayColor,
                  radius: 4.r,
                );
        },
        itemCount: widget.itemCount,
      ),
    );
  }
}

class RoundRectSwiperPagination extends StatefulWidget {
  ///当前的页面的下标
  final int currentIndex;

  ///总共几个页面
  final int itemCount;

  const RoundRectSwiperPagination(
      {Key? key, required this.currentIndex, required this.itemCount})
      : super(key: key);

  @override
  State<RoundRectSwiperPagination> createState() =>
      _RoundRectSwiperPaginationState();
}

class _RoundRectSwiperPaginationState extends State<RoundRectSwiperPagination>
    with HYAppTheme {
  late ScrollController _swiperPaginationController;

  @override
  void initState() {
    _swiperPaginationController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    _swiperPaginationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 25.w,
      child: GridView.builder(
        controller: _swiperPaginationController,
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: widget.itemCount,
            childAspectRatio: 5 / 2,
            crossAxisSpacing: 3.r),
        itemBuilder: (BuildContext context, int index) {
          return index == widget.currentIndex
              ? Container(
                  decoration: BoxDecoration(
                      color: norMainThemeColors,
                      borderRadius: BorderRadius.circular(10.r)),
                )
              : Container(
                  padding: EdgeInsets.symmetric(horizontal: 5.r),
                  decoration: BoxDecoration(
                      color: norGrayColor.withOpacity(.7),
                      borderRadius: BorderRadius.circular(10.r)),
                );
        },
        itemCount: widget.itemCount,
      ),
    );
  }
}
