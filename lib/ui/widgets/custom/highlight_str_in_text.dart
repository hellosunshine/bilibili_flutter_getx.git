import 'package:bilibili_getx/core/constant_util/size.dart';
import "package:flutter/material.dart";
import 'package:flutter_screenutil/flutter_screenutil.dart';

///将重要的文本在一长串文本中以一种颜色标出
class HYHighlightStrInText extends StatelessWidget with SizeConstantUtil {
  final String highlightText;
  final String originalText;
  final Color highlightColor;
  final Color originalColor;

  const HYHighlightStrInText(
      {Key? key,
      required this.highlightText,
      required this.originalText,
      required this.highlightColor,
      required this.originalColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> originalTextList = originalText.split('');
    List<InlineSpan> textSpans = [];
    for (var item in originalTextList) {
      if (highlightText.contains(item) == true) {
        textSpans.add(
          TextSpan(
            text: item,
            style: TextStyle(
              color: highlightColor,
              fontSize: titleFontSizeCons.r,
            ),
          ),
        );
      } else {
        textSpans.add(
          TextSpan(
            text: item,
            style: TextStyle(
              color: originalColor,
              fontSize: titleFontSizeCons.r,
            ),
          ),
        );
      }
    }
    return Text.rich(
      TextSpan(
        children: textSpans,
      ),
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
    );
  }
}
