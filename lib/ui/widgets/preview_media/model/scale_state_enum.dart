enum ScaleState {
  // ///初始状态
  // none,
  //
  // ///平移
  // pan,
  //
  // ///拖拽关闭
  // dragOut,
  //
  // ///返回原状
  // backToOriginal,
  //
  // ///双击放大
  // doubleTapScale,
  //
  // ///释放返回边界
  // releaseToEdge,
  //
  // ///原始大小
  // original
  ///原始大小
  originalSize,

  ///放大
  zoomedIn,

  ///缩小
  zoomedOut,
}
