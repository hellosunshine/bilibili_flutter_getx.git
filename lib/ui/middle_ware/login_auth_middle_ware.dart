import 'package:bilibili_getx/core/I18n/str_res_keys.dart';
import 'package:bilibili_getx/core/constant_util/app_theme.dart';
import 'package:bilibili_getx/core/constant_util/size.dart';
import 'package:bilibili_getx/core/shared_preferences/bilibili_shared_preference.dart';
import 'package:bilibili_getx/core/shared_preferences/shared_preference_util.dart';
import 'package:bilibili_getx/ui/pages/main/home/login/login_view.dart';
import 'package:bilibili_getx/ui/shared/image_asset.dart';
import 'package:bilibili_getx/ui/widgets/custom/rectangle_checkBox.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:get/get.dart';

//登录中间件
class LoginAuthMiddleWare extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    /// 是否登录
    bool isLogin =
        SharedPreferenceUtil.getBool(BilibiliSharedPreference.isLogin) ?? false;
    Future.delayed(const Duration(seconds: 1),
        () => Get.snackbar("提示", "请先登录，账号/密码:admin/admin"));
    return isLogin ? null : const RouteSettings(name: BilibiliLoginView.routeName);
  }
}

class LoginWidget with SizeConstantUtil, HYAppTheme, ImageAssets {
  void goToLogin() {
    SmartDialog.showLoading(
      displayTime: const Duration(seconds: 2),
      maskColor: const Color.fromRGBO(0, 0, 0, .4),
      builder: (ctx) {
        return Container(
          decoration: BoxDecoration(
            color: const Color.fromRGBO(0, 0, 0, .8),
            borderRadius: BorderRadius.all(
              Radius.circular(8.r),
            ),
          ),
          height: 80.r,
          width: 80.r,
          padding: const EdgeInsets.all(5).r,
          child: Column(
            children: [
              Image.asset(
                ploadingGif,
                width: 35.r,
                height: 35.r,
              ),
              10.verticalSpace,
              Text(
                SR.loading.tr,
                style: TextStyle(
                  color: norGrayColor,
                  fontSize: customFontSizeCons.r,
                  fontWeight: FontWeight.normal,
                ),
              )
            ],
          ),
        );
      },
    ).whenComplete(() {
      if (defaultTargetPlatform == TargetPlatform.android ||
          defaultTargetPlatform == TargetPlatform.iOS) {
        SmartDialog.dismiss();
        SmartDialog.show(
          builder: (ctx) {
            return buildHomeOneClickLogin();
          },
        );
      } else {
        Get.toNamed(BilibiliLoginView.routeName);
      }
    });
  }

  ///一键登录弹窗
  Widget buildHomeOneClickLogin() {
    ///是否同意协议
    var flag = true;
    return Card(
      child: Container(
        padding: EdgeInsets.all(10.r),
        width: 270.r,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.all(15.r),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    SR.login2UnlockMoreAmazingContent.tr,
                    style: TextStyle(
                      color: norTextColors,
                      fontSize: customFontSizeCons.r,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 30.r, bottom: 20.r),
                    child: Text(
                      "183****1731",
                      style: TextStyle(fontSize: 18.sp, color: norTextColors),
                    ),
                  ),
                  TextButton(
                    style: ButtonStyle(
                        padding: MaterialStateProperty.all(
                          EdgeInsets.symmetric(vertical: 10.r),
                        ),
                        backgroundColor:
                            MaterialStateProperty.all(norMainThemeColors)),
                    onPressed: () {
                      ///一键登录
                      // loginAuth();
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 2.r),
                      alignment: AlignmentDirectional.center,
                      width: 240.r,
                      child: Text(
                        SR.oneClickLogin.tr.toUpperCase(),
                        style: TextStyle(
                          color: norWhite01Color,
                          fontSize: titleFontSizeCons.r,
                          fontFamily: bF,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 15.r, bottom: 20.r),
                    child: GestureDetector(
                      onTap: () {
                        Get.toNamed(BilibiliLoginView.routeName);
                        SmartDialog.dismiss();
                      },
                      child: Text(
                        SR.otherWay.tr,
                        style:
                            TextStyle(fontSize: 14.sp, color: norGray04Color),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(right: 3.r, top: 3.r),
                        child: RectangleCheckBox(
                          ///自定义矩形的checkbox
                          size: 14.r,
                          checkedColor: norMainThemeColors,
                          isChecked: flag,
                          onTap: (value) {
                            flag = value!;
                          },
                        ),
                      ),
                      5.horizontalSpace,
                      buildHomeAgreement(),
                    ],
                  ),
                ],
              ),
            ),
            Positioned(
              right: 0,
              top: 0,
              child: GestureDetector(
                onTap: () {
                  SmartDialog.dismiss();
                },
                child: Icon(
                  Icons.close,
                  color: norGrayColor,
                  size: iconSizeCons.r,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ///用户协议
  Widget buildHomeAgreement() {
    return Expanded(
      child: Text.rich(
        TextSpan(
          children: [
            TextSpan(
              text: SR.userAgreementText01.tr,
              style: TextStyle(color: norGrayColor, fontSize: 10.r),
            ),
            TextSpan(
              text: SR.userAgreementText02.tr,
              style: TextStyle(color: norBlue01Colors, fontSize: 10.r),
            ),
            TextSpan(
              text: SR.userAgreementText03.tr,
              style: TextStyle(color: norGrayColor, fontSize: 10.r),
            ),
            TextSpan(
              text: SR.userAgreementText04.tr,
              style: TextStyle(color: norBlue01Colors, fontSize: 10.r),
            ),
            TextSpan(
              text: SR.userAgreementText05.tr,
              style: TextStyle(color: norGrayColor, fontSize: 10.r),
            ),
          ],
        ),
      ),
    );
  }
}
